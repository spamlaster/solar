<!DOCTYPE html>
<html lang="en">
	<head>
	    <!-- Meta, title, CSS, favicons, etc. -->
	    <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">
	
		<title>My Solar Living</title>
	
		<!-- Bootstrap core CSS -->
		<link href="assets/css/bootstrap.min.css" rel="stylesheet">
		
		<!-- Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,700,400italic,700italic' rel='stylesheet' type='text/css'>
		
		<!-- Rocket extras -->
		<link href="assets/css/animate.css" rel="stylesheet">
		<link href="assets/css/prettyPhoto.css" rel="stylesheet">
		<link href="assets/css/style.css" rel="stylesheet">
                <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/themes/base/jquery-ui.css" />
                
		<!-- Favicons -->
		<link rel="shortcut icon" href="../assets/favicon.png">
                
                <!-- Including jQuery and jQuery UI from Google's CDN -->
		<script src="http://code.jquery.com/jquery-1.7.1.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.16/jquery-ui.min.js"></script>
                <script src="jquery-1.11.3.min.js"></script>
                
     	<script type="text/javascript" src="jquery.cycle.all.js"></script>
	<script type="text/javascript">
		$('#testimonial > .container').cycle({ 
		    fx:     'scrollRight', 
		    delay:  1000,
			speed: 'slow',
			pause: 1,
			timeout: 5000,
			random: 1
		});
	</script>
	</head>
<style>
.form-control::-webkit-input-placeholder { color: #998; }
.form-control:-moz-placeholder { color: #998; }
.form-control::-moz-placeholder { color: #998; }
.form-control:-ms-input-placeholder { color: #998; }
option { color: #998; }
</style>
	<body>
	
	<!-- Start Rocket -->
	<!-- ********************* -->
	
	<!-- Parallax Background
	================================================== -->
	<!-- image is set in the CSS as a background image -->
        <div id="parallax">
            	
	<div id="testimonial">
		<div class="">
			<div class="pic1">
<!--                            <img src="http://i.huffpost.com/gen/1288056/images/o-SOLAR-PANELS-HOUSE-facebook.jpg" alt="" />-->
			</div>
			<div class="pic2">
                            <!--<img src="http://www.clocktowerrealty.com/wp-content/uploads/2014/07/Solar-Panels.jpg" alt=""  />-->	
			</div>
<!--			<div class="">
                            <img src="/assets/images/solar-power-generator.jpg" alt=""  />
				
			</div>-->
		</div>
	</div>
            
        </div>
	<!-- End Parallax Background
	================================================== -->
	
	<!-- Start Header
	================================================== -->
	<header id="header" class="navbar navbar-inverse navbar-fixed-top" role="banner">
	  <div class="container">
	    <div class="navbar-header">
	      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <!-- Your Logo -->
              <div style="height: 20px;">
              <a href="../" class="navbar-brand imglogo"><img  src="assets/images/solar_logo2.png"></a>
              </div>
            </div>
	    <!-- Start Navigation -->
	    <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
	      <ul class="nav navbar-nav">
	        <li>
	          <a href="#hero">Home</a>
	        </li>
	        <li>
	          <a href="#features">Why Solar</a>
	        </li>
	        <li>
	          <a href="contact/">About Us</a>
	        </li>
	      </ul>
	    </nav>
	  </div>
	</header>
	<!-- ==================================================
	End Header -->
	
	<!-- Start Hero Section
	================================================== -->
	<section id="hero" class="section">
		<div class="container">
			<div class="row">
                            <div class="col-md-5"><br>
                                <br>
                                <div class="lp-element animate" data-animate="fadeInDown">
                                    <br>
                                    <p class="lead"><strong>Save Money Go Solar!</strong>
                                    <br> Solar Panels are more affordable than ever. Learn how you can save by switching to solar!</p>
                                </div>
                            </div>
				<div class="col-md-7">
					<div class="lp-element animate" data-animate="fadeInDown">
<!--						<h1>Promo</h1> -->
						<p class="lead" style="font-size: 1.6em;"><strong>2015 Rebates Available Now!</strong>
						<br> Complete the form below to receive a free quote</p>
                                                    <form action="insert.php" method="post">
                                                    <div class="clearfix feild">
							<div class="row">
                                                            <div class="form-group col-md-6">
								<input type="text" class="form-control" name="first_name" placeholder="First Name" id="inputSuccess1" required value="" />
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <input type="text" class="form-control input-normal" name="last_name" placeholder="Last Name" id="inputSuccess1" required value="" />                                                                
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <input type="text" class="form-control input-normal" name="address" placeholder="Address" id="inputSuccess1" required value="" />                                                                
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <input type="text" class="form-control input-normal" name="city" placeholder="City" id="inputSuccess1" required value="" />                                                                
                                                            </div>                                     
                                                            <div class="form-group col-md-6">
                                                                <input type="text" class="form-control input-normal" name="state" placeholder="State" id="inputSuccess1" required value="" />                                                                
                                                            </div>                                          
                                                            <div class="form-group col-md-6">
                                                                <input type="text" class="form-control input-normal" name="zip" placeholder="Zip" id="inputSuccess1" required value="" />                                                                
                                                            </div>                                
                                                            <div class="form-group col-md-6">
                                                                <input type="email" class="form-control input-normal" name="email" placeholder="Email" id="inputSuccess1" required value="" />                                                                
                                                            </div>                   
                                                            <div class="form-group col-md-6">
                                                                <input type="phone" class="form-control input-normal" name="phone" placeholder="Phone" id="inputSuccess1" required value="" />                                                                
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <input type="text" class="form-control input-normal" name="elct_compy" placeholder="Electric Company" id="inputSuccess1" required value="" />                                                                
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <select type="text" class="form-control input-normal selectpicker" name="bill" placeholder="Monthly Electric Bill" id="inputSuccess1" required value="" >
                                                                    <option value="" >Monthly Electric Bill</option>
                                                                    <option value="150">$100 - $200</option>
                                                                    <option value="250">$200 - $300</option>
                                                                    <option value="350">$300 - $400</option>
                                                                    <option value="450">$400 - $500</option>
                                                                    <option value="550">$500 - $600</option>
                                                                    <option value="650">$600 - $700</option>
                                                                    <option value="750">$700 - $800</option>
                                                                    <option value="850">$800 - $900</option>
                                                                    <option value="950">$900 - $1000</option>
                                                                </select>
                                                            </div>
                                                            <div class="form-group col-md-6">
                                                                <select type="text" class="form-control input-normal selectpicker" name="shade" required>
                                                                    <option value="">Select Roof Shade</option>
                                                                    <option value="No Shade">No Shade</option>
                                                                    <option value="Little Shade">Little Shade</option>
                                                                    <option value="A Lot of Shade">A Lot of Shade</option>
                                                                </select>
                                                            </div>
                                                            
<!--                                                            <div class="col-md-6"> 
                                                                <label for="fader" calss="label label-default" style="color:#888">Roof Shade</label>
                                                                <input class="form-control input-normal slider slider-track slider-selection slider-handle ui-slider-tick-mark" type="range" min="0" max="10" step="5" value="0" id="fader"  oninput="outputUpdate(value)">
                                                            </div>-->
                                                            <script>

    function outputUpdate(vol) {
if (vol == 0) {
document.querySelector('#volume').value = 'No Shade';
}
if (vol == 5) {
document.querySelector('#volume').value = 'Some Shade';
}
if (vol == 10) {
document.querySelector('#volume').value = 'Most Shade';
}
}
document.querySelector('#volume').value = 'No Shade';

                                                                </script>
                                                                  
<!--                                                            <div class="form-group col-md-6" id="displaydiv">
                                                         <output class="col-md-6 form-control input-normal label label-default"  style="font-weight: 300; color:#888; border-color: #ecf0f1; background-color: #ecf0f1; font-size: 14px;" for="fader" id="volume">No shade</output>   
                                                            </div>-->
                                                                  
                                                            </div>  
                                                           
                                                            <div class="col-md-12 text-center">
                                                        <button class="btn btn-success btn-lg col-md-12" role="button">Qualify Now <span class="glyphicon glyphicon-arrow-right"></span></button>
                                                        
                                                            </div>
                                                        </div>
							</form>
							
<!--                                                            <hr>-->
                                                        
                                                        <div class="form-group" style="margin-top:15px;">
                                                            <p style="font-size: 12px;">By clicking submit, I agree to this <a href="privacy.php">Privacy Policy</a> and give consent to receive autodialed and/or pre-recorded calls from mysolarliving.com and four of these <a>solar partners</a> at the number I provided, even if it's a wireless number and/or is on a state or federal "Do Not Call" list. I understand that consent is not a condition of purchase.</p>
							</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- ================================================== 
	End Hero -->
	
	<!-- Start Features
	================================================== -->
	<section id="features" class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<!-- Title -->
					<div class="overview animate" data-animate="flipInX">
                                            <h1>NEW 2015 HOME SOLAR REBATES AVAILABLE!<br> FIND OUT HOW MUCH YOU CAN SAVE!</h1>
						<p class="lead">Solar is simple to sign up for and qualified homeowners pay $0 up front.</p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="service-block animate" data-animate="fadeInRight">
						<span class="glyphicon glyphicon-usd"></span>
						<h3>Save up to 80% on your electric bill</h3>
						<p>Once you start using Solar, it ends up paying for itself by saving you on electric bills.</p>
<!--						<button type="button" class="btn btn-primary btn-md btn-rocket btn-rocket-default">Learn More</button>-->
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="service-block animate" data-animate="flipInY">
						<span class="glyphicon glyphicon-usd"></span>
						<h3>Limited-time Government Rebates!</h3>
						<p>You can receive a 30% tax credit along with incentives, grants and rebates provided by various state governments.</p>
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="service-block animate" data-animate="fadeInLeft">
						<span class="glyphicon glyphicon-usd"></span>
						<h3>Leasing / Financing</h3>
						<p>With the introduction of 30% tax credits, you pay next to nothing to get started. You can lease panels for as little as $0 down.</p>
<!--						<button type="button" class="btn btn-primary btn-md btn-rocket btn-rocket-default">Learn More</button>-->
					</div>
				</div>
				<div class="col-md-3 col-sm-6">
					<div class="service-block animate" data-animate="flipInX">
						<span class="glyphicon glyphicon-usd"></span>
						<h3>Environment friendly</h3>
						<p>Solar energy is renewable and saves our planet from pollution and global warming.</p>
<!--						<button type="button" class="btn btn-primary btn-md btn-rocket btn-rocket-default">Learn More</button>-->
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<!-- ==================================================
	End Features -->
	
	
	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="assets/js/jquery-1.10.2.min.js"></script>
	<script src="assets/js/bootstrap.js"></script>
	<script src="assets/js/waypoints.min.js"></script>
	<script src="assets/js/jquery.scrollto.min.js"></script>
	<script src="assets/js/jquery.localscroll.min.js"></script>
	<script src="assets/js/jquery.prettyPhoto.js"></script>
	<script src="assets/js/scripts.js"></script>
	
	
	<!-- ********************* -->
	<!-- Start Rocket -->
	
	</body>
</html>
