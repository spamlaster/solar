<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Solar CV</title>
  <meta name="description" content="mobile first, app, web app, responsive, admin dashboard, flat, flat ui">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">	

<!--  <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" />-->
  <link rel="stylesheet" href="css/bootstrap.min.css" />
<!--=== INCLUDE SCRIPTS ===-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>  
<script src="js/bootstrap.min.js"></script>
<script type='text/javascript' src='//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js'></script>

<script type="text/javascript" src="jquery-1.8.1-min.js"></script>


<style>
b {
    width: 121px;
}
.btn-lg {
    font-size: 17px;
}
 b {
    width:150px;
}
</style>
    <body>
        
<div class="container-full">

      <div class="row">
       
        <div class="col-lg-12 text-center v-center">
          
          <h1>Call Verified</h1>
          <p class="lead">Solar Call Verified Lead Page</p>
          
          <br>
          
<!--          <form class="col-lg-12 form" role="form" action="insert.php" method="post" onSubmit="if(!confirm('Success!')){return false;}">-->
            <form class="col-lg-12 form" role="form" action="insert.php" method="post">
            <div class="input-group" style="width:340px;text-align:center;margin:0 auto;">
                <input class="form-control input-lg" placeholder="First Name" required="" type="text" name="firstName" value="<?php if(isset($_REQUEST['firstName'])){ echo $_REQUEST['firstName']; } ?>">
                <span class="input-group-btn"><b class="btn btn-lg btn-primary" >First Name</b></span>
            </div>
            <div class="input-group" style="width:340px;text-align:center;margin:0 auto;">
                <input class="form-control input-lg" placeholder="Last Name" required="" type="text" name="lastName" value="<?php if(isset($_REQUEST['lastName'])){ echo $_REQUEST['lastName']; } ?>">
                <span class="input-group-btn"><b class="btn btn-lg btn-primary" >Last Name</b></span>
            </div>
            <div class="input-group" style="width:340px;text-align:center;margin:0 auto;">
                <input class="form-control input-lg" title="Don't worry. We hate spam, and will not share your email with anyone." placeholder="Email address" required="" type="email" name="email" value="<?php if(isset($_REQUEST['email'])){ echo $_REQUEST['email']; } ?>">
                <span class="input-group-btn"><b class="btn btn-lg btn-primary" >Email</b></span>
            </div>
            <div class="input-group" style="width:340px;text-align:center;margin:0 auto;">
                <input class="form-control input-lg" placeholder="Phone Number" required="" name="homePhone" value="<?php if(isset($_REQUEST['homePhone'])){ echo $_REQUEST['homePhone']; } ?>">
                <span class="input-group-btn"><b class="btn btn-lg btn-primary" >Phone</b></span>
            </div>              
            <div class="input-group" style="width:340px;text-align:center;margin:0 auto;">
                <input class="form-control input-lg" placeholder="Address" required="" type="text" name="address" value="<?php if(isset($_REQUEST['address'])){ echo $_REQUEST['address']; } ?>">
                <span class="input-group-btn"><b class="btn btn-lg btn-primary" >Address</b></span>                
            </div>
 
<!--  <script>

    function is_int(value){ 
      if((parseFloat(value) == parseInt(value)) && !isNaN(value)){
        return true;
      } else { 
        return false;
      } 
    }

    $(function() {

      // Set up
      $(".fancy-form div > div").hide();
      var firstReveal = true;

      $("#zip").keyup(function() {

        // Cache 
        var el = $(this);

        // Did they type five integers?
        if ((el.val().length == 5) && (is_int(el.val())))  {

          // Call Ziptastic for information
          $.ajax({
            url: "http://ZiptasticAPI.com/" + el.val(),
            cache: false,
            dataType: "json",
            type: "GET",
            data: "zip=" + el.val(),
            success: function(result, success) {

              $(".zip-error, .instructions").slideUp(200);

              $("#city").val(result.city);

              $("#state").val(result.state);			

              $(".fancy-form div > div").slideDown();

              if (firstReveal) {
                // Ghetto but without this timeout the field doesn't
                // take focus for some reason.
                setTimeout(function() { $("#address-line-1").focus(); }, 
                  400);
              };

              firstReveal = false;		

            },
            error: function(result, success) {

              $(".zip-error").slideDown(300);

            }

          });

        } else if (el.val().length < 5) {

          $(".zip-error").slideUp(200);

        };

      });

    }); // END DOM Ready
  </script>-->
  

  <?php $state = $_GET['state']; ?>
      
              
            <div class="input-group" style="width:340px;text-align:center;margin:0 auto;">
                <input class="form-control input-lg" placeholder="Zip" required=""  maxlength="5" type="text" name="zip" id="zip" value="<?php if(isset($_REQUEST['zip'])){ echo $_REQUEST['zip']; } ?>">
                <span class="input-group-btn"><b class="btn btn-lg btn-primary">Zip Code</b></span> 
            </div>
            <div class="input-group" style="width:340px;text-align:center;margin:0 auto;">
                <input class="form-control input-lg" placeholder="City" required="" type="text" name="city" id="city" value="<?php if(isset($_REQUEST['city'])){ echo $_REQUEST['city']; } ?>">
                <span class="input-group-btn"><b class="btn btn-lg btn-primary">City</b></span> 
            </div>
            <div class="input-group" style="width:340px;text-align:center;margin:0 auto;">
                <select class="form-control input-lg" name="state" id="state">
                    <option>Select State</option>
                    <option value="AL" <?php if ($_REQUEST['state'] == "AL"){echo 'selected="selected"';}?> >Alabama</option>
                    <option value="AK" <?php if ($_REQUEST['state'] == 'AK'){echo 'selected="selected"';}?>>Alaska</option>
                    <option value="AZ" <?php if ($_REQUEST['state'] == 'AZ'){echo 'selected="selected"';}?>>Arizona</option>
                    <option value="AR" <?php if ($_REQUEST['state'] == 'AR'){echo 'selected="selected"';}?>>Arkansas</option>
                    <option value="CA" <?php if ($_REQUEST['state'] == 'CA'){echo 'selected="selected"';}?>>California</option>
                    <option value="CO" <?php if ($_REQUEST['state'] == 'CO'){echo 'selected="selected"';}?>>Colorado</option>
                    <option value="CT" <?php if ($_REQUEST['state'] == 'CT'){echo 'selected="selected"';}?>>Connecticut</option>
                    <option value="DE" <?php if ($_REQUEST['state'] == 'DE'){echo 'selected="selected"';}?>>Delaware</option>
                    <option value="DC" <?php if ($_REQUEST['state'] == 'DC'){echo 'selected="selected"';}?>>District Of Columbia</option>
                    <option value="FL" <?php if ($_REQUEST['state'] == 'FL'){echo 'selected="selected"';}?>>Florida</option>
                    <option value="GA" <?php if ($_REQUEST['state'] == 'GA'){echo 'selected="selected"';}?>>Georgia</option>
                    <option value="HI" <?php if ($_REQUEST['state'] == 'HI'){echo 'selected="selected"';}?>>Hawaii</option>
                    <option value="ID" <?php if ($_REQUEST['state'] == 'ID'){echo 'selected="selected"';}?>>Idaho</option>
                    <option value="IL" <?php if ($_REQUEST['state'] == 'IL'){echo 'selected="selected"';}?>>Illinois</option>
                    <option value="IN" <?php if ($_REQUEST['state'] == 'IN'){echo 'selected="selected"';}?>>Indiana</option>
                    <option value="IA" <?php if ($_REQUEST['state'] == 'IA'){echo 'selected="selected"';}?>>Iowa</option>
                    <option value="KS" <?php if ($_REQUEST['state'] == 'KS'){echo 'selected="selected"';}?>>Kansas</option>
                    <option value="KY" <?php if ($_REQUEST['state'] == 'KY'){echo 'selected="selected"';}?>>Kentucky</option>
                    <option value="LA" <?php if ($_REQUEST['state'] == 'LA'){echo 'selected="selected"';}?>>Louisiana</option>
                    <option value="ME" <?php if ($_REQUEST['state'] == 'ME'){echo 'selected="selected"';}?>>Maine</option>
                    <option value="MD" <?php if ($_REQUEST['state'] == 'MD'){echo 'selected="selected"';}?>>Maryland</option>
                    <option value="MA" <?php if ($_REQUEST['state'] == 'MA'){echo 'selected="selected"';}?>>Massachusetts</option>
                    <option value="MI" <?php if ($_REQUEST['state'] == 'MI'){echo 'selected="selected"';}?>>Michigan</option>
                    <option value="MN" <?php if ($_REQUEST['state'] == 'MN'){echo 'selected="selected"';}?>>Minnesota</option>
                    <option value="MS" <?php if ($_REQUEST['state'] == 'MS'){echo 'selected="selected"';}?>>Mississippi</option>
                    <option value="MO" <?php if ($_REQUEST['state'] == 'MO'){echo 'selected="selected"';}?>>Missouri</option>
                    <option value="MT" <?php if ($_REQUEST['state'] == 'MT'){echo 'selected="selected"';}?>>Montana</option>
                    <option value="NE" <?php if ($_REQUEST['state'] == 'NE'){echo 'selected="selected"';}?>>Nebraska</option>
                    <option value="NV" <?php if ($_REQUEST['state'] == 'NV'){echo 'selected="selected"';}?>>Nevada</option>
                    <option value="NH" <?php if ($_REQUEST['state'] == 'NH'){echo 'selected="selected"';}?>>New Hampshire</option>
                    <option value="NJ" <?php if ($_REQUEST['state'] == 'NJ'){echo 'selected="selected"';}?>>New Jersey</option>
                    <option value="NM" <?php if ($_REQUEST['state'] == 'NM'){echo 'selected="selected"';}?>>New Mexico</option>
                    <option value="NY" <?php if ($_REQUEST['state'] == 'NY'){echo 'selected="selected"';}?>>New York</option>
                    <option value="NC" <?php if ($_REQUEST['state'] == 'NC'){echo 'selected="selected"';}?>>North Carolina</option>
                    <option value="ND" <?php if ($_REQUEST['state'] == 'ND'){echo 'selected="selected"';}?>>North Dakota</option>
                    <option value="OH" <?php if ($_REQUEST['state'] == 'OH'){echo 'selected="selected"';}?>>Ohio</option>
                    <option value="OK" <?php if ($_REQUEST['state'] == 'OK'){echo 'selected="selected"';}?>>Oklahoma</option>
                    <option value="OR" <?php if ($_REQUEST['state'] == 'OR'){echo 'selected="selected"';}?>>Oregon</option>
                    <option value="PA" <?php if ($_REQUEST['state'] == 'PA'){echo 'selected="selected"';}?>>Pennsylvania</option>
                    <option value="RI" <?php if ($_REQUEST['state'] == 'RI'){echo 'selected="selected"';}?>>Rhode Island</option>
                    <option value="SC" <?php if ($_REQUEST['state'] == 'SC'){echo 'selected="selected"';}?>>South Carolina</option>
                    <option value="SD" <?php if ($_REQUEST['state'] == 'SD'){echo 'selected="selected"';}?>>South Dakota</option>
                    <option value="TN" <?php if ($_REQUEST['state'] == 'TN'){echo 'selected="selected"';}?>>Tennessee</option>
                    <option value="TX" <?php if ($_REQUEST['state'] == 'TX'){echo 'selected="selected"';}?>>Texas</option>
                    <option value="UT" <?php if ($_REQUEST['state'] == 'UT'){echo 'selected="selected"';}?>>Utah</option>
                    <option value="VT" <?php if ($_REQUEST['state'] == 'VT'){echo 'selected="selected"';}?>>Vermont</option>
                    <option value="WA" <?php if ($_REQUEST['state'] == 'WA'){echo 'selected="selected"';}?>>Washington</option>
                    <option value="WV" <?php if ($_REQUEST['state'] == 'WV'){echo 'selected="selected"';}?>>West Virginia</option>
                    <option value="WI" <?php if ($_REQUEST['state'] == 'WI'){echo 'selected="selected"';}?>>Wisconsin</option>
                    <option value="WY" <?php if ($_REQUEST['state'] == 'WY'){echo 'selected="selected"';}?>>Wyoming</option>
                </select>
                <span class="input-group-btn"><b class="btn btn-lg btn-primary" >State</b></span>
            </div>    
            <div class="input-group" style="width:340px;text-align:center;margin:0 auto;">
                <select class="form-control input-lg" required="" name="electricBill">
                    <option selected="selected">Electric Bill</option>
                    <option value="0-50">$0-50</option>
                    <option value="51-100">$51-100</option>
                    <option value="101-150">$101-150</option>
                    <option value="151-200">$151-200</option>
                    <option value="201-300">$201-300</option>
                    <option value="301-400">$301-400</option>
                    <option value="401-500">$401-500</option>
                    <option value="501-600">$501-600</option>
                    <option value="601-700">$601-700</option>
                    <option value="701-800">$701-800</option>
                    <option value="801+">$801+</option>
                </select>
                <span class="input-group-btn"><b class="btn btn-lg btn-primary" >Electric Bill</b></span>
            </div>
            <div class="input-group" style="width:340px;text-align:center;margin:0 auto;">
                <select class="form-control input-lg" required="" name="curt_utl" id="curt_utl">
                    <option value="">Current Electricity Provider</option>
                                <optgroup label="Alaska">
                                        <option value="Alaska Villages Electric Cooperative">Alaska Villages Electric Cooperative</option>
                                            <option value="Chugach Electric Association">Chugach Electric Association</option>
                                            <option value="Copper Valley Electric Association">Copper Valley Electric Association</option>
                                            <option value="Golden Valley Electric Association">Golden Valley Electric Association</option>
                                            <option value="Ketchikan Public Utilities (KPU)">Ketchikan Public Utilities (KPU)</option>
                                            <option value="Kodiak Electric Association">Kodiak Electric Association</option>
                                            <option value="Matanuska Electric Association">Matanuska Electric Association</option>
                                            <option value="Municipal Light & Power">Municipal Light & Power</option>
                                            <option value="Sitka Electric">Sitka Electric</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Alabama">
                                        <option value="AECO (Alabama Electric Company)">AECO (Alabama Electric Company)</option>
                                            <option value="Alabama Power">Alabama Power</option>
                                            <option value="Alabama Rural Electric Association">Alabama Rural Electric Association</option>
                                            <option value="Baldwin EMC">Baldwin EMC</option>
                                            <option value="Central Alabama Electric Co-Op">Central Alabama Electric Co-Op</option>
                                            <option value="Cherokee Electric Cooperative">Cherokee Electric Cooperative</option>
                                            <option value="City of Athens Utilities">City of Athens Utilities</option>
                                            <option value="City of Bessemer">City of Bessemer</option>
                                            <option value="City of Bessemer Electric and Water Service">City of Bessemer Electric and Water Service</option>
                                            <option value="Covington Electric Cooperative">Covington Electric Cooperative</option>
                                            <option value="Huntsville Utilities">Huntsville Utilities</option>
                                            <option value="North Alabama Electric Coop">North Alabama Electric Coop</option>
                                            <option value="PowerSouth Energy Cooperative">PowerSouth Energy Cooperative</option>
                                            <option value="Riviera Utilities">Riviera Utilities</option>
                                            <option value="Sand Mountain Electric Co-Op">Sand Mountain Electric Co-Op</option>
                                            <option value="Sheffield Utilities">Sheffield Utilities</option>
                                            <option value="Srd Electrical Services">Srd Electrical Services</option>
                                            <option value="Tallapoosa River Electric Cooperative (TREC)">Tallapoosa River Electric Cooperative (TREC)</option>
                                            <option value="Tennessee Valley Authority">Tennessee Valley Authority</option>
                                            <option value="Wiregrass Electric Cooperative">Wiregrass Electric Cooperative</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Arkansas">
                                        <option value="Arkansas Valley Electric Cooperative">Arkansas Valley Electric Cooperative</option>
                                            <option value="C and L Electric Co-Op Corporation">C and L Electric Co-Op Corporation</option>
                                            <option value="Carroll Electric Cooperative Corporation">Carroll Electric Cooperative Corporation</option>
                                            <option value="Entergy">Entergy</option>
                                            <option value="First Electric Cooperative">First Electric Cooperative</option>
                                            <option value="North Arkansas Electric Cooperative">North Arkansas Electric Cooperative</option>
                                            <option value="North Little Rock Electric">North Little Rock Electric</option>
                                            <option value="Ozarks Electric Cooperative">Ozarks Electric Cooperative</option>
                                            <option value="Paragould Light Water and Cable (PLWC)">Paragould Light Water and Cable (PLWC)</option>
                                            <option value="Petit Jean Electric Cooperative">Petit Jean Electric Cooperative</option>
                                            <option value="Spark Energy">Spark Energy</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Arizona">
                                        <option value="Amanti Electric">Amanti Electric</option>
                                            <option value="Arizona Public Service">Arizona Public Service</option>
                                            <option value="City of Mesa Utilities">City of Mesa Utilities</option>
                                            <option value="City of Safford Electric Department">City of Safford Electric Department</option>
                                            <option value="Duncan Valley Electric Cooperative">Duncan Valley Electric Cooperative</option>
                                            <option value="Electrical District No. 2">Electrical District No. 2</option>
                                            <option value="Electrical District No. 3">Electrical District No. 3</option>
                                            <option value="Graham County Utilities">Graham County Utilities</option>
                                            <option value="Mojave Electric Cooperative">Mojave Electric Cooperative</option>
                                            <option value="Navopache Electric Cooperative">Navopache Electric Cooperative</option>
                                            <option value="Salt River Project">Salt River Project</option>
                                            <option value="Sulphur Springs Valley Electric Cooperative">Sulphur Springs Valley Electric Cooperative</option>
                                            <option value="Trico Electric Cooperative">Trico Electric Cooperative</option>
                                            <option value="Tucson Electric Power">Tucson Electric Power</option>
                                            <option value="UniSource Energy Services">UniSource Energy Services</option>
                                            <option value="Xcel Energy">Xcel Energy</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="California">
                                        <option value="Alameda Municipal Power">Alameda Municipal Power</option>
                                            <option value="Ambit Energy">Ambit Energy</option>
                                            <option value="Anaheim Public Utilities">Anaheim Public Utilities</option>
                                            <option value="Anza Electric Co-Op">Anza Electric Co-Op</option>
                                            <option value="Azusa Light & Water">Azusa Light & Water</option>
                                            <option value="Bear Valley Electric">Bear Valley Electric</option>
                                            <option value="Burbank Water & Power">Burbank Water & Power</option>
                                            <option value="California Public Utilities Commission">California Public Utilities Commission</option>
                                            <option value="City & County of San Francisco">City & County of San Francisco</option>
                                            <option value="City of Banning Electric">City of Banning Electric</option>
                                            <option value="City of Lompoc Utilities">City of Lompoc Utilities</option>
                                            <option value="City of Palo Alto Utilities">City of Palo Alto Utilities</option>
                                            <option value="City of Roseville Electric">City of Roseville Electric</option>
                                            <option value="City of Shasta Lake Electric Utility">City of Shasta Lake Electric Utility</option>
                                            <option value="Colton Public Utilities Glendale Public Service Department">Colton Public Utilities Glendale Public Service Department</option>
                                            <option value="El Cerrito Electric">El Cerrito Electric</option>
                                            <option value="Glendale Water and Power">Glendale Water and Power</option>
                                            <option value="Gridley Municipal Utilities">Gridley Municipal Utilities</option>
                                            <option value="Healdsburg Municipal Electric Department">Healdsburg Municipal Electric Department</option>
                                            <option value="IGS Energy">IGS Energy</option>
                                            <option value="Imperial Irrigation District">Imperial Irrigation District</option>
                                            <option value="Lodi Electric Utility">Lodi Electric Utility</option>
                                            <option value="Los Angeles Department of Water and Power">Los Angeles Department of Water and Power</option>
                                            <option value="Modesto Irrigation District">Modesto Irrigation District</option>
                                            <option value="Mountain Utilities">Mountain Utilities</option>
                                            <option value="Pacific Gas & Electric">Pacific Gas & Electric</option>
                                            <option value="PacifiCorp">PacifiCorp</option>
                                            <option value="Pasadena Water & Power">Pasadena Water & Power</option>
                                            <option value="Plumas Sierra Rural Electric Cooperative">Plumas Sierra Rural Electric Cooperative</option>
                                            <option value="Redding Electric Utility (REU)">Redding Electric Utility (REU)</option>
                                            <option value="Riverside Public Utilities">Riverside Public Utilities</option>
                                            <option value="Roseville Electric">Roseville Electric</option>
                                            <option value="Sacramento Municipal Utility District">Sacramento Municipal Utility District</option>
                                            <option value="San Diego Gas & Electric">San Diego Gas & Electric</option>
                                            <option value="Sierra-Pacific Power">Sierra-Pacific Power</option>
                                            <option value="Silicon Valley Power">Silicon Valley Power</option>
                                            <option value="Southern California Edison">Southern California Edison</option>
                                            <option value="Southern California Public Power Authority">Southern California Public Power Authority</option>
                                            <option value="Surprise Valley Power">Surprise Valley Power</option>
                                            <option value="Trinity PUD">Trinity PUD</option>
                                            <option value="Turlock Irrigation District (TID)">Turlock Irrigation District (TID)</option>
                                            <option value="Valley Electric Association">Valley Electric Association</option>
                                            <option value="Vernon Light & Power">Vernon Light & Power</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Colorado">
                                        <option value="Bedell Electric">Bedell Electric</option>
                                            <option value="Black Hills Energy">Black Hills Energy</option>
                                            <option value="City of Fort Collins Utilities">City of Fort Collins Utilities</option>
                                            <option value="City of Loveland Water and Power">City of Loveland Water and Power</option>
                                            <option value="City of Wauchula Electric">City of Wauchula Electric</option>
                                            <option value="Colorado Springs Utilities">Colorado Springs Utilities</option>
                                            <option value="Delta-Montrose Electric Association">Delta-Montrose Electric Association</option>
                                            <option value="Grand Valley Power">Grand Valley Power</option>
                                            <option value="GreyStone Power Corporation">GreyStone Power Corporation</option>
                                            <option value="Gunnison County Electric Company">Gunnison County Electric Company</option>
                                            <option value="Highline Electric Association">Highline Electric Association</option>
                                            <option value="Holy Cross Energy Company">Holy Cross Energy Company</option>
                                            <option value="Intermountain Rural Electric Association">Intermountain Rural Electric Association</option>
                                            <option value="Kit Carson Electric Cooperative">Kit Carson Electric Cooperative</option>
                                            <option value="La Plata Electric Association">La Plata Electric Association</option>
                                            <option value="Lamar Light and Power">Lamar Light and Power</option>
                                            <option value="Longmont Power and Communications">Longmont Power and Communications</option>
                                            <option value="Moon Lake Electric">Moon Lake Electric</option>
                                            <option value="Morgan County Rural Electric Association">Morgan County Rural Electric Association</option>
                                            <option value="Mountain View Electric Association">Mountain View Electric Association</option>
                                            <option value="Poudre Valley Rural Electric Association">Poudre Valley Rural Electric Association</option>
                                            <option value="San Isabel Electric Association">San Isabel Electric Association</option>
                                            <option value="San Luis Valley Rural Electric">San Luis Valley Rural Electric</option>
                                            <option value="San Miguel Power Association">San Miguel Power Association</option>
                                            <option value="Southeast Colorado Power Association">Southeast Colorado Power Association</option>
                                            <option value="United Power Inc">United Power Inc</option>
                                            <option value="Xcel Energy">Xcel Energy</option>
                                            <option value="Yampa Valley Electric Association">Yampa Valley Electric Association</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Connecticut">
                                        <option value="Abest Power & Gas">Abest Power & Gas</option>
                                            <option value="Ambit Energy">Ambit Energy</option>
                                            <option value="Bozrah Light and Power">Bozrah Light and Power</option>
                                            <option value="Connecticut Light & Power">Connecticut Light & Power</option>
                                            <option value="Connecticut Natural Gas">Connecticut Natural Gas</option>
                                            <option value="Eversource Energy">Eversource Energy</option>
                                            <option value="Groton Utilities">Groton Utilities</option>
                                            <option value="Northeast Utilities">Northeast Utilities</option>
                                            <option value="United Illuminating">United Illuminating</option>
                                            <option value="Wallingford Electric">Wallingford Electric</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="District of Columbia">
                                        <option value="Ambit Energy">Ambit Energy</option>
                                            <option value="PEPCO">PEPCO</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Delaware">
                                        <option value="Calpine">Calpine</option>
                                            <option value="City of Dover Electric Department">City of Dover Electric Department</option>
                                            <option value="City of Milford Electric Department">City of Milford Electric Department</option>
                                            <option value="Delaware Electric Cooperative">Delaware Electric Cooperative</option>
                                            <option value="Delmarva Power">Delmarva Power</option>
                                            <option value="NRG Energy">NRG Energy</option>
                                            <option value="Town of Middletown Utilities">Town of Middletown Utilities</option>
                                            <option value="Town of Smyrna">Town of Smyrna</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Florida">
                                        <option value="Baugh Southeast Co-Op">Baugh Southeast Co-Op</option>
                                            <option value="Choctawhatchee Electric Co-Op">Choctawhatchee Electric Co-Op</option>
                                            <option value="City of Bartow Utility">City of Bartow Utility</option>
                                            <option value="City of Leesburg">City of Leesburg</option>
                                            <option value="City of New Smyrna Beach">City of New Smyrna Beach</option>
                                            <option value="City of Tallahassee Utilities">City of Tallahassee Utilities</option>
                                            <option value="City of Vero Beach">City of Vero Beach</option>
                                            <option value="Clay Electric Co-op">Clay Electric Co-op</option>
                                            <option value="Duke Energy">Duke Energy</option>
                                            <option value="Florida Keys Electric Cooperative">Florida Keys Electric Cooperative</option>
                                            <option value="Florida Municipal Power Agency">Florida Municipal Power Agency</option>
                                            <option value="Florida Power & Light">Florida Power & Light</option>
                                            <option value="Florida Public Utility Company Palm Beach">Florida Public Utility Company Palm Beach</option>
                                            <option value="Gainesville Regional Utilities">Gainesville Regional Utilities</option>
                                            <option value="Glades Electric Cooperative">Glades Electric Cooperative</option>
                                            <option value="Gulf Power">Gulf Power</option>
                                            <option value="JEA">JEA</option>
                                            <option value="Kissimmee Utility Authority">Kissimmee Utility Authority</option>
                                            <option value="Lake Worth Utilities">Lake Worth Utilities</option>
                                            <option value="Lakeland Electric">Lakeland Electric</option>
                                            <option value="LCEC">LCEC</option>
                                            <option value="Ocala Electric">Ocala Electric</option>
                                            <option value="Okefenokee Rural Electric (OREMC)">Okefenokee Rural Electric (OREMC)</option>
                                            <option value="Orlando Utilities Commission">Orlando Utilities Commission</option>
                                            <option value="Paige Electric">Paige Electric</option>
                                            <option value="Peace River Electric Cooperative">Peace River Electric Cooperative</option>
                                            <option value="Progress Energy Florida">Progress Energy Florida</option>
                                            <option value="SECO Energy">SECO Energy</option>
                                            <option value="St. Petersburg Utility Department">St. Petersburg Utility Department</option>
                                            <option value="Talquin Electric Cooperative">Talquin Electric Cooperative</option>
                                            <option value="Tampa Electric">Tampa Electric</option>
                                            <option value="TECO">TECO</option>
                                            <option value="West Florida Electric Cooperative">West Florida Electric Cooperative</option>
                                            <option value="Withlacoochee River Electric Cooperative">Withlacoochee River Electric Cooperative</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Georgia">
                                        <option value="Amicalola Electric Membership Corporation">Amicalola Electric Membership Corporation</option>
                                            <option value="Blue Ridge Mountain EMC">Blue Ridge Mountain EMC</option>
                                            <option value="Charles E Hill Electric Co">Charles E Hill Electric Co</option>
                                            <option value="City of LaGrange Utilities">City of LaGrange Utilities</option>
                                            <option value="City of Sylvester Utilities">City of Sylvester Utilities</option>
                                            <option value="Cobb EMC">Cobb EMC</option>
                                            <option value="Colquitt Electric Corporation">Colquitt Electric Corporation</option>
                                            <option value="Coweta Fayette EMC">Coweta Fayette EMC</option>
                                            <option value="Diverse Power">Diverse Power</option>
                                            <option value="Fitzgerald Utilities">Fitzgerald Utilities</option>
                                            <option value="Flint Energies">Flint Energies</option>
                                            <option value="Georgia EMC">Georgia EMC</option>
                                            <option value="Georgia Power">Georgia Power</option>
                                            <option value="Habersham EMC">Habersham EMC</option>
                                            <option value="Hart EMC">Hart EMC</option>
                                            <option value="Jackson EMC">Jackson EMC</option>
                                            <option value="North Georgia EMC">North Georgia EMC</option>
                                            <option value="Rowell Electric Co">Rowell Electric Co</option>
                                            <option value="Sawnee EMC">Sawnee EMC</option>
                                            <option value="Snapping Shoals EMC">Snapping Shoals EMC</option>
                                            <option value="Southern Rivers Energy">Southern Rivers Energy</option>
                                            <option value="Tennessee Valley Authority">Tennessee Valley Authority</option>
                                            <option value="Tri-State Electric">Tri-State Electric</option>
                                            <option value="Walton EMC">Walton EMC</option>
                                            <option value="Washington EMC">Washington EMC</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Hawaii">
                                        <option value="Hawaiian Electric Company (HECO)">Hawaiian Electric Company (HECO)</option>
                                            <option value="Hawaiian Electric Industries">Hawaiian Electric Industries</option>
                                            <option value="Kauai Island Utility Cooperative (KIUC)">Kauai Island Utility Cooperative (KIUC)</option>
                                            <option value="Maui Electric Company">Maui Electric Company</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Iowa">
                                        <option value="Alliant Energy">Alliant Energy</option>
                                            <option value="Chariton Valley Electric Cooperative">Chariton Valley Electric Cooperative</option>
                                            <option value="City of Denver Electric (Iowa)">City of Denver Electric (Iowa)</option>
                                            <option value="FRANKLIN REC">FRANKLIN REC</option>
                                            <option value="Guthrie County REC">Guthrie County REC</option>
                                            <option value="Guttenberg Public Works and Utilities">Guttenberg Public Works and Utilities</option>
                                            <option value="Interstate Power and Light Company">Interstate Power and Light Company</option>
                                            <option value="Linn County RE">Linn County RE</option>
                                            <option value="MidAmerican Energy">MidAmerican Energy</option>
                                            <option value="Midland Power Co-Op">Midland Power Co-Op</option>
                                            <option value="Muscatine Power and Water">Muscatine Power and Water</option>
                                            <option value="Southwest Iowa Rural Electric Cooperative">Southwest Iowa Rural Electric Cooperative</option>
                                            <option value="Tip Rural Electric Co-Op">Tip Rural Electric Co-Op</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Idaho">
                                        <option value="City of Rupert Electric">City of Rupert Electric</option>
                                            <option value="City of Weiser">City of Weiser</option>
                                            <option value="Clearwater Power">Clearwater Power</option>
                                            <option value="IDACORP">IDACORP</option>
                                            <option value="Idaho Power">Idaho Power</option>
                                            <option value="Kootenai Electric Cooperative">Kootenai Electric Cooperative</option>
                                            <option value="Lost River Electric Co-Op">Lost River Electric Co-Op</option>
                                            <option value="Northern Lights Electric Cooperative">Northern Lights Electric Cooperative</option>
                                            <option value="PacifiCorp (Rocky Mountain Power)">PacifiCorp (Rocky Mountain Power)</option>
                                            <option value="Riverside Electric Co">Riverside Electric Co</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Illinois">
                                        <option value="Ambit Energy">Ambit Energy</option>
                                            <option value="Ameren">Ameren</option>
                                            <option value="Batavia Municipal Electric">Batavia Municipal Electric</option>
                                            <option value="City of Peru">City of Peru</option>
                                            <option value="City of Princeton Electric">City of Princeton Electric</option>
                                            <option value="City of St. Charles">City of St. Charles</option>
                                            <option value="ComEd Electric Company">ComEd Electric Company</option>
                                            <option value="Corn Belt Energy Corporation">Corn Belt Energy Corporation</option>
                                            <option value="Egyptian Electric">Egyptian Electric</option>
                                            <option value="Gexpro Electric">Gexpro Electric</option>
                                            <option value="IGS Energy">IGS Energy</option>
                                            <option value="Jo-Carroll Energy">Jo-Carroll Energy</option>
                                            <option value="Norris Electric Cooperative">Norris Electric Cooperative</option>
                                            <option value="Red Bud Utilities Plant">Red Bud Utilities Plant</option>
                                            <option value="Southeastern Illinois Electric Coop">Southeastern Illinois Electric Coop</option>
                                            <option value="Spoon River Electric Cooperative">Spoon River Electric Cooperative</option>
                                            <option value="Springfield City Water Light & Power">Springfield City Water Light & Power</option>
                                            <option value="Tara Energy">Tara Energy</option>
                                            <option value="Village of Hanover Park Municipal Light and Power">Village of Hanover Park Municipal Light and Power</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Indiana">
                                        <option value="American Electric Power">American Electric Power</option>
                                            <option value="Duke Energy Indiana">Duke Energy Indiana</option>
                                            <option value="Garrett Electric Utilities">Garrett Electric Utilities</option>
                                            <option value="Hendricks Power Cooperative">Hendricks Power Cooperative</option>
                                            <option value="Henry County REMC">Henry County REMC</option>
                                            <option value="IGS Energy">IGS Energy</option>
                                            <option value="Indiana Michigan Power">Indiana Michigan Power</option>
                                            <option value="Indianapolis Power & Light">Indianapolis Power & Light</option>
                                            <option value="Jackson County REMC">Jackson County REMC</option>
                                            <option value="Lebanon Utilities">Lebanon Utilities</option>
                                            <option value="Meade Electric Company">Meade Electric Company</option>
                                            <option value="Miami-Cass County REMC">Miami-Cass County REMC</option>
                                            <option value="Mishawaka Utilities">Mishawaka Utilities</option>
                                            <option value="Noble REMC">Noble REMC</option>
                                            <option value="Northern Indiana Public Service Company">Northern Indiana Public Service Company</option>
                                            <option value="South Central Indiana REMC">South Central Indiana REMC</option>
                                            <option value="Southeastern Indiana REMC">Southeastern Indiana REMC</option>
                                            <option value="Vectren">Vectren</option>
                                            <option value="Whitewater Valley REMC">Whitewater Valley REMC</option>
                                            <option value="WIN Energy REMC">WIN Energy REMC</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Kansas">
                                        <option value="Bluestem Electric">Bluestem Electric</option>
                                            <option value="Caney Valley Elec Co-Op Association">Caney Valley Elec Co-Op Association</option>
                                            <option value="Coffeyville Municipal Light and Power">Coffeyville Municipal Light and Power</option>
                                            <option value="Kansas City Board of Public Utilities">Kansas City Board of Public Utilities</option>
                                            <option value="Kansas City Power & Light">Kansas City Power & Light</option>
                                            <option value="Midwest Energy">Midwest Energy</option>
                                            <option value="Rolling Hills Electric Cooperative">Rolling Hills Electric Cooperative</option>
                                            <option value="Westar Energy">Westar Energy</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Kentucky">
                                        <option value="American Electric Power">American Electric Power</option>
                                            <option value="Berea Municipal Utilities">Berea Municipal Utilities</option>
                                            <option value="Big Rivers Electric Corporation">Big Rivers Electric Corporation</option>
                                            <option value="Blue Grass Energy">Blue Grass Energy</option>
                                            <option value="Bowling Green Municipal Utilities">Bowling Green Municipal Utilities</option>
                                            <option value="Clark Energy Cooperative">Clark Energy Cooperative</option>
                                            <option value="Duke Energy Kentucky">Duke Energy Kentucky</option>
                                            <option value="Fleming-Mason Energy Cooperative">Fleming-Mason Energy Cooperative</option>
                                            <option value="Frankfort Plant Board">Frankfort Plant Board</option>
                                            <option value="Grayson Rural Electric Electric Cooperative">Grayson Rural Electric Electric Cooperative</option>
                                            <option value="Henderson Municipal Power and Light">Henderson Municipal Power and Light</option>
                                            <option value="IGS Energy">IGS Energy</option>
                                            <option value="Kenergy Corp">Kenergy Corp</option>
                                            <option value="Kentucky Utilities">Kentucky Utilities</option>
                                            <option value="Louisville Gas & Electric">Louisville Gas & Electric</option>
                                            <option value="Nolin Rural Electric Cooperative">Nolin Rural Electric Cooperative</option>
                                            <option value="Owen Electric">Owen Electric</option>
                                            <option value="Owensboro Municipal Utilities">Owensboro Municipal Utilities</option>
                                            <option value="Pennyrile Rural Electric Co-Op">Pennyrile Rural Electric Co-Op</option>
                                            <option value="Salt River Electric">Salt River Electric</option>
                                            <option value="Shelby Energy Cooperative">Shelby Energy Cooperative</option>
                                            <option value="Tennessee Valley Authority">Tennessee Valley Authority</option>
                                            <option value="West Kentucky Rural Electric Cooperative Corporation">West Kentucky Rural Electric Cooperative Corporation</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Louisiana">
                                        <option value="Beauregard Electric CO-OP">Beauregard Electric CO-OP</option>
                                            <option value="CITY OF ALEXANDRIA">CITY OF ALEXANDRIA</option>
                                            <option value="CITY OF MINDEN">CITY OF MINDEN</option>
                                            <option value="Claiborne Electric Cooperative">Claiborne Electric Cooperative</option>
                                            <option value="CLECO">CLECO</option>
                                            <option value="Concordia Electric Co-Op">Concordia Electric Co-Op</option>
                                            <option value="DEMCO">DEMCO</option>
                                            <option value="East-Central Iowa Rural Electric Cooperative">East-Central Iowa Rural Electric Cooperative</option>
                                            <option value="Entergy">Entergy</option>
                                            <option value="Etheredge Electric Co">Etheredge Electric Co</option>
                                            <option value="Heartland Rural Electric Cooperative">Heartland Rural Electric Cooperative</option>
                                            <option value="Jefferson Davis Electric Co-Op">Jefferson Davis Electric Co-Op</option>
                                            <option value="Lafayette Utilities System">Lafayette Utilities System</option>
                                            <option value="Pointe Coupee Electric Membership">Pointe Coupee Electric Membership</option>
                                            <option value="Puckett Electric">Puckett Electric</option>
                                            <option value="SLECMO">SLECMO</option>
                                            <option value="SWEPCO">SWEPCO</option>
                                            <option value="Town of Vinton Electric">Town of Vinton Electric</option>
                                            <option value="Washington - St. Tammany Electric Cooperative">Washington - St. Tammany Electric Cooperative</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Massachusetts">
                                        <option value="Ambit Energy">Ambit Energy</option>
                                            <option value="Belmont Electric Light Department">Belmont Electric Light Department</option>
                                            <option value="Berkshire Company">Berkshire Company</option>
                                            <option value="Braintree Electric Light Department">Braintree Electric Light Department</option>
                                            <option value="Chicopee Electric Light Department">Chicopee Electric Light Department</option>
                                            <option value="City of Fitchburg">City of Fitchburg</option>
                                            <option value="Danvers Electric">Danvers Electric</option>
                                            <option value="Eversource Energy">Eversource Energy</option>
                                            <option value="Hingham Light Department">Hingham Light Department</option>
                                            <option value="Holden Municipal Light Department">Holden Municipal Light Department</option>
                                            <option value="Holyoke Gas and Electric">Holyoke Gas and Electric</option>
                                            <option value="Hudson Light and Power">Hudson Light and Power</option>
                                            <option value="Ipswich Utility Department">Ipswich Utility Department</option>
                                            <option value="Mansfield Municipal Electric">Mansfield Municipal Electric</option>
                                            <option value="Marblehead Electric Light Department">Marblehead Electric Light Department</option>
                                            <option value="Massachusetts Electric">Massachusetts Electric</option>
                                            <option value="Massachusetts Municipal Wholesale Electric Company (MMWEC)">Massachusetts Municipal Wholesale Electric Company (MMWEC)</option>
                                            <option value="Middleborough Gas and Electric">Middleborough Gas and Electric</option>
                                            <option value="Nantucket Electric">Nantucket Electric</option>
                                            <option value="National Grid">National Grid</option>
                                            <option value="North Attleborough Electric Department">North Attleborough Electric Department</option>
                                            <option value="Northeast Utilities">Northeast Utilities</option>
                                            <option value="Norwood Municipal Light and Power">Norwood Municipal Light and Power</option>
                                            <option value="NSTAR">NSTAR</option>
                                            <option value="Paxton Municipal Light Department">Paxton Municipal Light Department</option>
                                            <option value="Peabody Municipal Light Plant">Peabody Municipal Light Plant</option>
                                            <option value="Reading Municipal Light Department">Reading Municipal Light Department</option>
                                            <option value="Rowley Electric Light Department">Rowley Electric Light Department</option>
                                            <option value="SELCO">SELCO</option>
                                            <option value="Sterling Municipal Light Department">Sterling Municipal Light Department</option>
                                            <option value="Taunton Municipal Lighting Plant (TMLP)">Taunton Municipal Lighting Plant (TMLP)</option>
                                            <option value="Templeton Municipal Light Co">Templeton Municipal Light Co</option>
                                            <option value="Viridian">Viridian</option>
                                            <option value="Wakefield Municipal Gas and Light Department">Wakefield Municipal Gas and Light Department</option>
                                            <option value="Western Massachusetts Electric">Western Massachusetts Electric</option>
                                            <option value="Westfield Gas and Electric Department">Westfield Gas and Electric Department</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Maryland">
                                        <option value="Allegheny Power">Allegheny Power</option>
                                            <option value="Ambit Energy">Ambit Energy</option>
                                            <option value="Baltimore Gas & Electric">Baltimore Gas & Electric</option>
                                            <option value="Choptank Electric Cooperative">Choptank Electric Cooperative</option>
                                            <option value="Constellation Energy">Constellation Energy</option>
                                            <option value="Delmarva Power">Delmarva Power</option>
                                            <option value="IGS Energy">IGS Energy</option>
                                            <option value="Kleppinger Electric Co">Kleppinger Electric Co</option>
                                            <option value="PEPCO">PEPCO</option>
                                            <option value="Potomoc Edison (FirstEnergy)">Potomoc Edison (FirstEnergy)</option>
                                            <option value="Southern Maryland Electric Cooperative">Southern Maryland Electric Cooperative</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Maine">
                                        <option value="Bangor Hydro Electric">Bangor Hydro Electric</option>
                                            <option value="Central Maine Power">Central Maine Power</option>
                                            <option value="Eastern Maine Electric Cooperative">Eastern Maine Electric Cooperative</option>
                                            <option value="Kennebunk Light and Power">Kennebunk Light and Power</option>
                                            <option value="Maine Public Utilities Commission">Maine Public Utilities Commission</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Michigan">
                                        <option value="American Electric Power">American Electric Power</option>
                                            <option value="City Of Eaton Rapids">City Of Eaton Rapids</option>
                                            <option value="Consumers Energy">Consumers Energy</option>
                                            <option value="DTE Energy (Detroit Edison)">DTE Energy (Detroit Edison)</option>
                                            <option value="Excel Energy">Excel Energy</option>
                                            <option value="Holland Board of Public Works">Holland Board of Public Works</option>
                                            <option value="IGS Energy">IGS Energy</option>
                                            <option value="Indiana Michigan Power">Indiana Michigan Power</option>
                                            <option value="Lansing Board of Water & Light">Lansing Board of Water & Light</option>
                                            <option value="Marshall Electric and Waterworks">Marshall Electric and Waterworks</option>
                                            <option value="Midwest Energy Cooperative">Midwest Energy Cooperative</option>
                                            <option value="TC Electric">TC Electric</option>
                                            <option value="Upper Peninsula Power Company">Upper Peninsula Power Company</option>
                                            <option value="Wakefield Municipal Gas and Light">Wakefield Municipal Gas and Light</option>
                                            <option value="We Energies">We Energies</option>
                                            <option value="Wyandotte Municipal Services">Wyandotte Municipal Services</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Minnesota">
                                        <option value="Alexandra Light and Power">Alexandra Light and Power</option>
                                            <option value="Alliant Energy">Alliant Energy</option>
                                            <option value="Austin Public Utilities">Austin Public Utilities</option>
                                            <option value="Basin Electric Power Cooperative">Basin Electric Power Cooperative</option>
                                            <option value="Connexus Energy">Connexus Energy</option>
                                            <option value="Crown Electric Inc.">Crown Electric Inc.</option>
                                            <option value="Dakota Electric Association">Dakota Electric Association</option>
                                            <option value="East Central Energy">East Central Energy</option>
                                            <option value="East Grand Forks Electric and Water">East Grand Forks Electric and Water</option>
                                            <option value="East River Electric Power Co-op">East River Electric Power Co-op</option>
                                            <option value="Federated Rural Electric">Federated Rural Electric</option>
                                            <option value="Goodhue County Co-Op Electric">Goodhue County Co-Op Electric</option>
                                            <option value="Grand Marais Public Utilities">Grand Marais Public Utilities</option>
                                            <option value="Great River Energy">Great River Energy</option>
                                            <option value="Hibbing Public Utilities (HPUC)">Hibbing Public Utilities (HPUC)</option>
                                            <option value="Hibbing Public Utilities (HPUC)">Hibbing Public Utilities (HPUC)</option>
                                            <option value="Hutchinson Utilities Commission">Hutchinson Utilities Commission</option>
                                            <option value="Itasca-Mantrap Co-op Electrical Association">Itasca-Mantrap Co-op Electrical Association</option>
                                            <option value="ITC Midwest">ITC Midwest</option>
                                            <option value="Janesville Municipal Utility">Janesville Municipal Utility</option>
                                            <option value="L&O Power Co-op">L&O Power Co-op</option>
                                            <option value="Lake Country Power">Lake Country Power</option>
                                            <option value="Lake Region Electric Coop">Lake Region Electric Coop</option>
                                            <option value="Lake Region Electric Coop">Lake Region Electric Coop</option>
                                            <option value="Marshall Municipal Utilities">Marshall Municipal Utilities</option>
                                            <option value="Meeker Cooperative">Meeker Cooperative</option>
                                            <option value="Mille Lacs Energy Co-Op">Mille Lacs Energy Co-Op</option>
                                            <option value="Minnesota Power">Minnesota Power</option>
                                            <option value="Minnesota Valley Electric Cooperative">Minnesota Valley Electric Cooperative</option>
                                            <option value="Minnkota Power Cooperative">Minnkota Power Cooperative</option>
                                            <option value="Missouri River Energy Services">Missouri River Energy Services</option>
                                            <option value="Moorhead City Public Services Department">Moorhead City Public Services Department</option>
                                            <option value="New Ulm Public Utilities">New Ulm Public Utilities</option>
                                            <option value="North Star Electric Coop">North Star Electric Coop</option>
                                            <option value="Northern States Power Company (NSP)">Northern States Power Company (NSP)</option>
                                            <option value="Otter Tail Power Company">Otter Tail Power Company</option>
                                            <option value="Owatonna Public Utilities">Owatonna Public Utilities</option>
                                            <option value="People">People</option>
                                            <option value="PKM Electric Cooperative (PKMcoop)">PKM Electric Cooperative (PKMcoop)</option>
                                            <option value="Renville-Sibley Coop">Renville-Sibley Coop</option>
                                            <option value="Rochester Public Utilities">Rochester Public Utilities</option>
                                            <option value="Roseau Electric Cooperative">Roseau Electric Cooperative</option>
                                            <option value="Runestone Electric Association">Runestone Electric Association</option>
                                            <option value="Shakopee Public Utilities">Shakopee Public Utilities</option>
                                            <option value="South Central Electric Association (SCEA)">South Central Electric Association (SCEA)</option>
                                            <option value="Sterns Electric Association">Sterns Electric Association</option>
                                            <option value="Tri-County Electric">Tri-County Electric</option>
                                            <option value="Wild Rice Electric Cooperative">Wild Rice Electric Cooperative</option>
                                            <option value="Xcel Energy">Xcel Energy</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Missouri">
                                        <option value="Ameren">Ameren</option>
                                            <option value="Barton County Electric Cooperative">Barton County Electric Cooperative</option>
                                            <option value="Black River Electric Cooperative">Black River Electric Cooperative</option>
                                            <option value="Boone Electric Cooperative">Boone Electric Cooperative</option>
                                            <option value="Callaway Electric Cooperative">Callaway Electric Cooperative</option>
                                            <option value="Citizens Electric Corporation">Citizens Electric Corporation</option>
                                            <option value="City of Waynesville Electric">City of Waynesville Electric</option>
                                            <option value="City Utilities of Springfield">City Utilities of Springfield</option>
                                            <option value="Coast Electric">Coast Electric</option>
                                            <option value="Crawford Electric Cooperative">Crawford Electric Cooperative</option>
                                            <option value="Cuivre River Electric">Cuivre River Electric</option>
                                            <option value="Empire District Electric Company">Empire District Electric Company</option>
                                            <option value="Gascosage Electric Co-Op">Gascosage Electric Co-Op</option>
                                            <option value="Hannibal Board of Public Works">Hannibal Board of Public Works</option>
                                            <option value="Harrisonville Electric">Harrisonville Electric</option>
                                            <option value="Howard Electric Cooperative">Howard Electric Cooperative</option>
                                            <option value="Howell-Oregon Electric Co-Op">Howell-Oregon Electric Co-Op</option>
                                            <option value="Independence Power and Light">Independence Power and Light</option>
                                            <option value="Intercounty Electric Cooperative">Intercounty Electric Cooperative</option>
                                            <option value="Kansas City Power & Light">Kansas City Power & Light</option>
                                            <option value="Laclede Electric Cooperative">Laclede Electric Cooperative</option>
                                            <option value="New-Mac Electric">New-Mac Electric</option>
                                            <option value="Nixa Utilities">Nixa Utilities</option>
                                            <option value="Ozark Border Electric Cooperative">Ozark Border Electric Cooperative</option>
                                            <option value="Platte-Clay Electric Cooperative">Platte-Clay Electric Cooperative</option>
                                            <option value="Rolla Municipal Utilities (RMU)">Rolla Municipal Utilities (RMU)</option>
                                            <option value="SAC Osage Electric Co-Op">SAC Osage Electric Co-Op</option>
                                            <option value="Webster Electric Cooperative">Webster Electric Cooperative</option>
                                            <option value="West Central Electric Cooperative">West Central Electric Cooperative</option>
                                            <option value="White Valley River Valley Electric Cooperative">White Valley River Valley Electric Cooperative</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Mississippi">
                                        <option value="Coahoma EPA">Coahoma EPA</option>
                                            <option value="East Mississippi Electric Power Association">East Mississippi Electric Power Association</option>
                                            <option value="Entergy">Entergy</option>
                                            <option value="Magnolia Electric">Magnolia Electric</option>
                                            <option value="Mississippi Power Company">Mississippi Power Company</option>
                                            <option value="Prentiss County Electric Power Association (PCEPA)">Prentiss County Electric Power Association (PCEPA)</option>
                                            <option value="Singing River Electric Power Association">Singing River Electric Power Association</option>
                                            <option value="Southern Pine Electric Power Association">Southern Pine Electric Power Association</option>
                                            <option value="Southwest Mississippi Electric Power Association">Southwest Mississippi Electric Power Association</option>
                                            <option value="Tennessee Valley Authority">Tennessee Valley Authority</option>
                                            <option value="Tishomingo County Power">Tishomingo County Power</option>
                                            <option value="Tombigbee Electric Power Association">Tombigbee Electric Power Association</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Montana">
                                        <option value="Central Montana Electric Power Cooperative">Central Montana Electric Power Cooperative</option>
                                            <option value="Flathead Electric Co-Op">Flathead Electric Co-Op</option>
                                            <option value="MDU">MDU</option>
                                            <option value="Mission Valley Power">Mission Valley Power</option>
                                            <option value="Missoula Electric Cooperative">Missoula Electric Cooperative</option>
                                            <option value="Montana Electric Cooperatives Association">Montana Electric Cooperatives Association</option>
                                            <option value="Northwestern Energy">Northwestern Energy</option>
                                            <option value="Park Electric Co-Op">Park Electric Co-Op</option>
                                            <option value="Ravalli Electric Co-Op">Ravalli Electric Co-Op</option>
                                            <option value="Vigilante Electric Co-Op">Vigilante Electric Co-Op</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="North Carolina">
                                        <option value="Albemarle Electric Membership Corporation">Albemarle Electric Membership Corporation</option>
                                            <option value="Ashley Smith Electric Co">Ashley Smith Electric Co</option>
                                            <option value="Blue Ridge Electrical Membership Corp.">Blue Ridge Electrical Membership Corp.</option>
                                            <option value="Brunswick EMC">Brunswick EMC</option>
                                            <option value="Cape Hatteras Electric Corporation">Cape Hatteras Electric Corporation</option>
                                            <option value="Carteret-Craven Electric Cooperative">Carteret-Craven Electric Cooperative</option>
                                            <option value="Central Electric Membership Corporation">Central Electric Membership Corporation</option>
                                            <option value="City of Albemarle">City of Albemarle</option>
                                            <option value="City of Kinston Electric">City of Kinston Electric</option>
                                            <option value="City of Lexington Utilities">City of Lexington Utilities</option>
                                            <option value="City of New Bern Electric">City of New Bern Electric</option>
                                            <option value="City of Shelby Utilities">City of Shelby Utilities</option>
                                            <option value="Dobbins Electric Co">Dobbins Electric Co</option>
                                            <option value="Dominion North Carolina Power">Dominion North Carolina Power</option>
                                            <option value="Duke Energy NC">Duke Energy NC</option>
                                            <option value="Edgecombe-Martin County EMC">Edgecombe-Martin County EMC</option>
                                            <option value="ElectriCities">ElectriCities</option>
                                            <option value="EnergyUnited Electric">EnergyUnited Electric</option>
                                            <option value="Farmville Utilities Department">Farmville Utilities Department</option>
                                            <option value="Four County Electric Membership">Four County Electric Membership</option>
                                            <option value="French Broad Electric Membership Corporation">French Broad Electric Membership Corporation</option>
                                            <option value="Greenville Utilities Commission">Greenville Utilities Commission</option>
                                            <option value="Halifax Electric Membership">Halifax Electric Membership</option>
                                            <option value="Haywood EMC">Haywood EMC</option>
                                            <option value="High Point Electric Department">High Point Electric Department</option>
                                            <option value="Jones-Onslow EMC">Jones-Onslow EMC</option>
                                            <option value="Lumbee River EMC">Lumbee River EMC</option>
                                            <option value="Lumberton Power">Lumberton Power</option>
                                            <option value="New River Light and Power">New River Light and Power</option>
                                            <option value="North Carolina Electric Membership Corp.">North Carolina Electric Membership Corp.</option>
                                            <option value="Piedmont Electric">Piedmont Electric</option>
                                            <option value="Progress Energy Carolinas">Progress Energy Carolinas</option>
                                            <option value="PWC Fayetteville">PWC Fayetteville</option>
                                            <option value="Randolph Electric Membership Corp">Randolph Electric Membership Corp</option>
                                            <option value="Roanoke Electric Co-Op">Roanoke Electric Co-Op</option>
                                            <option value="Rutherford EMC">Rutherford EMC</option>
                                            <option value="South River Electric Cooperative">South River Electric Cooperative</option>
                                            <option value="Storm Electric">Storm Electric</option>
                                            <option value="Surry-Yadkin EMC">Surry-Yadkin EMC</option>
                                            <option value="Tideland EMC">Tideland EMC</option>
                                            <option value="Town of Forest City Utilities">Town of Forest City Utilities</option>
                                            <option value="Union Power Cooperative">Union Power Cooperative</option>
                                            <option value="Wake Electric">Wake Electric</option>
                                            <option value="Washington Electric Utilities">Washington Electric Utilities</option>
                                            <option value="Wesco Electric">Wesco Electric</option>
                                            <option value="Williams Electric Co">Williams Electric Co</option>
                                            <option value="WIlson Energy">WIlson Energy</option>
                                            <option value="Wilson Utilities Department">Wilson Utilities Department</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="North Dakota">
                                        <option value="Basin Electric Power Cooperative">Basin Electric Power Cooperative</option>
                                            <option value="Central Power Electric Cooperative">Central Power Electric Cooperative</option>
                                            <option value="MDU">MDU</option>
                                            <option value="Minnkota Power Cooperative">Minnkota Power Cooperative</option>
                                            <option value="Otter Tail Power Company">Otter Tail Power Company</option>
                                            <option value="Upper Missouri G&T Cooperative">Upper Missouri G&T Cooperative</option>
                                            <option value="Verendrye Electric Cooperative">Verendrye Electric Cooperative</option>
                                            <option value="Xcel Energy">Xcel Energy</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Nebraska">
                                        <option value="ABC Electric Co">ABC Electric Co</option>
                                            <option value="City of Alliance">City of Alliance</option>
                                            <option value="Fairbury Light and Water">Fairbury Light and Water</option>
                                            <option value="Lincoln Electric System (LES)">Lincoln Electric System (LES)</option>
                                            <option value="Nebraska Public Power District">Nebraska Public Power District</option>
                                            <option value="Omaha Public Power District">Omaha Public Power District</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="New Hampshire">
                                        <option value="Eversource Energy">Eversource Energy</option>
                                            <option value="Granite State Electric">Granite State Electric</option>
                                            <option value="Liberty Utilities">Liberty Utilities</option>
                                            <option value="National Grid">National Grid</option>
                                            <option value="New Hampshire Electric Co-op (NHEC)">New Hampshire Electric Co-op (NHEC)</option>
                                            <option value="Northeast Utilities">Northeast Utilities</option>
                                            <option value="Public Service of NH">Public Service of NH</option>
                                            <option value="Unitil Corporation">Unitil Corporation</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="New Jersey">
                                        <option value="Ambit Energy">Ambit Energy</option>
                                            <option value="Asbury Park Electric Supply Co">Asbury Park Electric Supply Co</option>
                                            <option value="Atlantic City Electric">Atlantic City Electric</option>
                                            <option value="Baltimore Gas and Electric">Baltimore Gas and Electric</option>
                                            <option value="FirstEnergy">FirstEnergy</option>
                                            <option value="Gateway Energy Services">Gateway Energy Services</option>
                                            <option value="Jersey Central Power and Light Company">Jersey Central Power and Light Company</option>
                                            <option value="Milltown Boro Electric">Milltown Boro Electric</option>
                                            <option value="Northeast Utilities">Northeast Utilities</option>
                                            <option value="Orange & Rockland">Orange & Rockland</option>
                                            <option value="Powers Electric Company">Powers Electric Company</option>
                                            <option value="Public Service Electric and Gas Company (PSE&G)">Public Service Electric and Gas Company (PSE&G)</option>
                                            <option value="Sussex Rural Electric Cooperative">Sussex Rural Electric Cooperative</option>
                                            <option value="Vineland Municipal Electric Utility">Vineland Municipal Electric Utility</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="New Mexico">
                                        <option value="Central New Mexico Electric Cooperative">Central New Mexico Electric Cooperative</option>
                                            <option value="Columbus Electric Co-Op">Columbus Electric Co-Op</option>
                                            <option value="El Paso Electric">El Paso Electric</option>
                                            <option value="Farmington Electric Utility System">Farmington Electric Utility System</option>
                                            <option value="Jemez Mountains Electric Cooperative">Jemez Mountains Electric Cooperative</option>
                                            <option value="Kit Carson Electric">Kit Carson Electric</option>
                                            <option value="Lea County Electric Cooperative">Lea County Electric Cooperative</option>
                                            <option value="Navajo Tribal Utility Authority">Navajo Tribal Utility Authority</option>
                                            <option value="New Mexico Gas Company">New Mexico Gas Company</option>
                                            <option value="Northern Rio Arriba Electric Cooperative">Northern Rio Arriba Electric Cooperative</option>
                                            <option value="PNM">PNM</option>
                                            <option value="Public Service Company of New Mexico">Public Service Company of New Mexico</option>
                                            <option value="Springer  Electric Coop">Springer  Electric Coop</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Nevada">
                                        <option value="Lincoln County Power District Number 1">Lincoln County Power District Number 1</option>
                                            <option value="Nevada Power">Nevada Power</option>
                                            <option value="NV Energy">NV Energy</option>
                                            <option value="Overton Power District No 5">Overton Power District No 5</option>
                                            <option value="Sierra Pacific Power">Sierra Pacific Power</option>
                                            <option value="Surprise Valley Electric">Surprise Valley Electric</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="New York">
                                        <option value="Albany Water Gas and Light Commission">Albany Water Gas and Light Commission</option>
                                            <option value="Ambit Energy">Ambit Energy</option>
                                            <option value="Azure Mountain Power Co">Azure Mountain Power Co</option>
                                            <option value="Central Hudson Gas & Electric">Central Hudson Gas & Electric</option>
                                            <option value="Citizens Choice Energy Jamestown BPU">Citizens Choice Energy Jamestown BPU</option>
                                            <option value="Consolidated Edison Company of New York (Con Edison)">Consolidated Edison Company of New York (Con Edison)</option>
                                            <option value="Freeport Electric">Freeport Electric</option>
                                            <option value="Green Island Power Authority">Green Island Power Authority</option>
                                            <option value="IGS Energy">IGS Energy</option>
                                            <option value="Jamestown Board of Public Utilities">Jamestown Board of Public Utilities</option>
                                            <option value="Long Island Power Authority (LIPA)">Long Island Power Authority (LIPA)</option>
                                            <option value="Massena Electric Department">Massena Electric Department</option>
                                            <option value="National Grid">National Grid</option>
                                            <option value="New York Power Authority (NYPA)">New York Power Authority (NYPA)</option>
                                            <option value="New York State Electric & Gas (NYSEG)">New York State Electric & Gas (NYSEG)</option>
                                            <option value="Ocean Electric">Ocean Electric</option>
                                            <option value="Orange & Rockland">Orange & Rockland</option>
                                            <option value="Plattsburgh Municipal Lighting Department">Plattsburgh Municipal Lighting Department</option>
                                            <option value="Public Service Electric and Gas Company (PSE&G)">Public Service Electric and Gas Company (PSE&G)</option>
                                            <option value="Rochester Gas & Electric">Rochester Gas & Electric</option>
                                            <option value="Salamanca Public Utilities">Salamanca Public Utilities</option>
                                            <option value="Sherburne Municipal Electric">Sherburne Municipal Electric</option>
                                            <option value="Tupper Lake Municipal Electric Department">Tupper Lake Municipal Electric Department</option>
                                            <option value="Village of Akron Electric">Village of Akron Electric</option>
                                            <option value="Village of Lake Placid">Village of Lake Placid</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Ohio">
                                        <option value="American Electric Power">American Electric Power</option>
                                            <option value="Butler Rural Electric Cooperative">Butler Rural Electric Cooperative</option>
                                            <option value="Cincinnati Bell Energy">Cincinnati Bell Energy</option>
                                            <option value="Cleveland Electric Illuminating Company">Cleveland Electric Illuminating Company</option>
                                            <option value="Consolidated Electric Cooperative">Consolidated Electric Cooperative</option>
                                            <option value="Dayton Power & Light">Dayton Power & Light</option>
                                            <option value="Duke Energy Ohio">Duke Energy Ohio</option>
                                            <option value="Firelands Electric Cooperative">Firelands Electric Cooperative</option>
                                            <option value="FirstEnergy">FirstEnergy</option>
                                            <option value="Guernsey-Muskingum Electric Cooperative">Guernsey-Muskingum Electric Cooperative</option>
                                            <option value="Hamilton Utility Services Department">Hamilton Utility Services Department</option>
                                            <option value="Hancock Wood Electric Cooperative">Hancock Wood Electric Cooperative</option>
                                            <option value="Holmes-Wayne Electric Cooperative">Holmes-Wayne Electric Cooperative</option>
                                            <option value="IGS Energy">IGS Energy</option>
                                            <option value="Jess Howard Electric">Jess Howard Electric</option>
                                            <option value="Logan County Electric Cooperative">Logan County Electric Cooperative</option>
                                            <option value="Lorain Medina Rural Electric Cooperative">Lorain Medina Rural Electric Cooperative</option>
                                            <option value="Marshallville Utilities">Marshallville Utilities</option>
                                            <option value="Minster Utilities">Minster Utilities</option>
                                            <option value="Ohio Edison">Ohio Edison</option>
                                            <option value="Paulding Putnam Electric Cooperative">Paulding Putnam Electric Cooperative</option>
                                            <option value="South Central Power Company">South Central Power Company</option>
                                            <option value="The Energy Coop">The Energy Coop</option>
                                            <option value="Toledo Edison">Toledo Edison</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Oklahoma">
                                        <option value="American Electric Power">American Electric Power</option>
                                            <option value="Caddo Electric Cooperative">Caddo Electric Cooperative</option>
                                            <option value="City Of Waynoka Utility">City Of Waynoka Utility</option>
                                            <option value="Compton Electric Company">Compton Electric Company</option>
                                            <option value="Cotton Electric Co-Op">Cotton Electric Co-Op</option>
                                            <option value="Indian Electric Co-op">Indian Electric Co-op</option>
                                            <option value="Kiamichi Electric Co-Op">Kiamichi Electric Co-Op</option>
                                            <option value="Oklahoma Gas & Electric">Oklahoma Gas & Electric</option>
                                            <option value="Public Service Company of Oklahoma">Public Service Company of Oklahoma</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Oregon">
                                        <option value="Ameren">Ameren</option>
                                            <option value="Ashland Electric">Ashland Electric</option>
                                            <option value="Canby Electric">Canby Electric</option>
                                            <option value="Central Lincoln PUD">Central Lincoln PUD</option>
                                            <option value="City of Forest Grove Light and Power">City of Forest Grove Light and Power</option>
                                            <option value="Columbia River Public Utility District">Columbia River Public Utility District</option>
                                            <option value="Coos-Curry Electric Co-Op">Coos-Curry Electric Co-Op</option>
                                            <option value="Emerald PUD">Emerald PUD</option>
                                            <option value="Eugene Water & Electric Board">Eugene Water & Electric Board</option>
                                            <option value="Idaho Power">Idaho Power</option>
                                            <option value="Midstate Electric Cooperative">Midstate Electric Cooperative</option>
                                            <option value="PacifiCorp (Pacific Power)">PacifiCorp (Pacific Power)</option>
                                            <option value="Portland General Electric">Portland General Electric</option>
                                            <option value="Salem Electric">Salem Electric</option>
                                            <option value="Springfield Utility Board (SUB)">Springfield Utility Board (SUB)</option>
                                            <option value="Tillamook PUD">Tillamook PUD</option>
                                            <option value="Umatilla Electric Cooperative">Umatilla Electric Cooperative</option>
                                            <option value="Wasco Electric">Wasco Electric</option>
                                            <option value="West Oregon Electric Cooperative">West Oregon Electric Cooperative</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Pennsylvania">
                                        <option value="Adams Electric Cooperative">Adams Electric Cooperative</option>
                                            <option value="Allegheny Power">Allegheny Power</option>
                                            <option value="Ambit Energy">Ambit Energy</option>
                                            <option value="Borough of Ellwood City">Borough of Ellwood City</option>
                                            <option value="Borough of Ellwood City Electric">Borough of Ellwood City Electric</option>
                                            <option value="Citizens Electric of Lewisburg">Citizens Electric of Lewisburg</option>
                                            <option value="Claverack REC">Claverack REC</option>
                                            <option value="Duquesne Light">Duquesne Light</option>
                                            <option value="FirstEnergy">FirstEnergy</option>
                                            <option value="IGS Energy">IGS Energy</option>
                                            <option value="Lansdale Borough Electric">Lansdale Borough Electric</option>
                                            <option value="Met-Ed">Met-Ed</option>
                                            <option value="PECO">PECO</option>
                                            <option value="Penelec">Penelec</option>
                                            <option value="Penn Power">Penn Power</option>
                                            <option value="Pike County Light & Power Company">Pike County Light & Power Company</option>
                                            <option value="Powers Electric Company">Powers Electric Company</option>
                                            <option value="PPL">PPL</option>
                                            <option value="Rural Valley Electric Co.">Rural Valley Electric Co.</option>
                                            <option value="Superior Plus Utility">Superior Plus Utility</option>
                                            <option value="UGI Utilities">UGI Utilities</option>
                                            <option value="Warren Electric Co-Op">Warren Electric Co-Op</option>
                                            <option value="Wellsboro Electric Company">Wellsboro Electric Company</option>
                                            <option value="West Penn Power">West Penn Power</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Puerto Rico">
                                        <option value="Puerto Rico Electric Power Authority (PREPA)">Puerto Rico Electric Power Authority (PREPA)</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Rhode Island">
                                        <option value="Narragansett Electric">Narragansett Electric</option>
                                            <option value="National Grid">National Grid</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="South Carolina">
                                        <option value="Aiken Electric Co-op">Aiken Electric Co-op</option>
                                            <option value="Berkeley Electric Cooperative">Berkeley Electric Cooperative</option>
                                            <option value="Blue Ridge Electric Cooperative">Blue Ridge Electric Cooperative</option>
                                            <option value="Broad River Electric Cooperative">Broad River Electric Cooperative</option>
                                            <option value="Central Electric Power Cooperative">Central Electric Power Cooperative</option>
                                            <option value="City of Rock Hill">City of Rock Hill</option>
                                            <option value="Duke Energy SC">Duke Energy SC</option>
                                            <option value="Fairfield Electric Co-op">Fairfield Electric Co-op</option>
                                            <option value="Greer Commission of Public Works">Greer Commission of Public Works</option>
                                            <option value="Horry Electric Co-Op">Horry Electric Co-Op</option>
                                            <option value="Laurens Electric Cooperative">Laurens Electric Cooperative</option>
                                            <option value="Little River Electric Co-Op">Little River Electric Co-Op</option>
                                            <option value="Lynches River Electric Co-op">Lynches River Electric Co-op</option>
                                            <option value="Mid-Carolina Electric Coop">Mid-Carolina Electric Coop</option>
                                            <option value="Newberry Electric Cooperative">Newberry Electric Cooperative</option>
                                            <option value="Palmetto Electric">Palmetto Electric</option>
                                            <option value="Palmetto Electric Cooperative">Palmetto Electric Cooperative</option>
                                            <option value="Pee Dee Electric Cooperative">Pee Dee Electric Cooperative</option>
                                            <option value="Progress Energy Carolinas">Progress Energy Carolinas</option>
                                            <option value="Santee Cooper">Santee Cooper</option>
                                            <option value="South Carolina Electric & Gas Company">South Carolina Electric & Gas Company</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="South Dakota">
                                        <option value="Black Hills Power">Black Hills Power</option>
                                            <option value="East River Electric Cooperative">East River Electric Cooperative</option>
                                            <option value="Montana-Dakota Utilities">Montana-Dakota Utilities</option>
                                            <option value="Northwestern Energy">Northwestern Energy</option>
                                            <option value="Otter Tail Power Company">Otter Tail Power Company</option>
                                            <option value="Rushmore Electric Cooperative">Rushmore Electric Cooperative</option>
                                            <option value="West River Electric Association">West River Electric Association</option>
                                            <option value="Xcel Energy">Xcel Energy</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Tennessee">
                                        <option value="Alcoa Electric Department">Alcoa Electric Department</option>
                                            <option value="Athens Utilities Board">Athens Utilities Board</option>
                                            <option value="Bill Dower's Sons Electric Co">Bill Dower's Sons Electric Co</option>
                                            <option value="Chickasaw Electric Co-Op">Chickasaw Electric Co-Op</option>
                                            <option value="Citizens Utilities Board">Citizens Utilities Board</option>
                                            <option value="City of Cookeville Electric Department">City of Cookeville Electric Department</option>
                                            <option value="City of Lebanon Utilities">City of Lebanon Utilities</option>
                                            <option value="Clarksville Department of Electricity">Clarksville Department of Electricity</option>
                                            <option value="Clinton Utilities Board">Clinton Utilities Board</option>
                                            <option value="Columbia Power and Water Systems">Columbia Power and Water Systems</option>
                                            <option value="Cumberland Electric Membership Corporation">Cumberland Electric Membership Corporation</option>
                                            <option value="Dickson Electric System">Dickson Electric System</option>
                                            <option value="Duck River EMC">Duck River EMC</option>
                                            <option value="Electric Power Board">Electric Power Board</option>
                                            <option value="Elizabethton Electric Department">Elizabethton Electric Department</option>
                                            <option value="Forked Deer Electric">Forked Deer Electric</option>
                                            <option value="Fort Loudoun Electric Cooperative">Fort Loudoun Electric Cooperative</option>
                                            <option value="Gibson Electric Membership Corporation">Gibson Electric Membership Corporation</option>
                                            <option value="Holston Electric Cooperative (HEC)">Holston Electric Cooperative (HEC)</option>
                                            <option value="Jackson Energy Authority">Jackson Energy Authority</option>
                                            <option value="Jellico Electric and Water">Jellico Electric and Water</option>
                                            <option value="Kingsport Power (Appalachian Power)">Kingsport Power (Appalachian Power)</option>
                                            <option value="Knoxville Utilities Board">Knoxville Utilities Board</option>
                                            <option value="Landon Electric Company">Landon Electric Company</option>
                                            <option value="Lawrenceburg Utility Department">Lawrenceburg Utility Department</option>
                                            <option value="Lenoir City Utilities Board">Lenoir City Utilities Board</option>
                                            <option value="McMinnville Electric System">McMinnville Electric System</option>
                                            <option value="Memphis Light Gas and Water">Memphis Light Gas and Water</option>
                                            <option value="Middle Tennessee Electric Membership Corporation (MTEMC)">Middle Tennessee Electric Membership Corporation (MTEMC)</option>
                                            <option value="Mountain Electric Co-Op">Mountain Electric Co-Op</option>
                                            <option value="Nashville Electric Service">Nashville Electric Service</option>
                                            <option value="Rains Electric Company">Rains Electric Company</option>
                                            <option value="Sequachee Valley Electric Cooperative">Sequachee Valley Electric Cooperative</option>
                                            <option value="Sevier County Electric System">Sevier County Electric System</option>
                                            <option value="Sewell Electric Company">Sewell Electric Company</option>
                                            <option value="Tennessee Jackson Energy Authority">Tennessee Jackson Energy Authority</option>
                                            <option value="Tennessee Valley Authority">Tennessee Valley Authority</option>
                                            <option value="Trenton Light and Water">Trenton Light and Water</option>
                                            <option value="Upper Cumberland Electric Membership Corporation (UCEMC)">Upper Cumberland Electric Membership Corporation (UCEMC)</option>
                                            <option value="Volunteer Electric Cooperative">Volunteer Electric Cooperative</option>
                                            <option value="Weakley County Municipal Electric System">Weakley County Municipal Electric System</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Texas">
                                        <option value="Ambit Energy">Ambit Energy</option>
                                            <option value="American Electric Power">American Electric Power</option>
                                            <option value="Amigo Energy">Amigo Energy</option>
                                            <option value="AP Gas and Electric">AP Gas and Electric</option>
                                            <option value="Austin Energy">Austin Energy</option>
                                            <option value="Bandera Electric Cooperative">Bandera Electric Cooperative</option>
                                            <option value="Bartlett Electric Cooperative">Bartlett Electric Cooperative</option>
                                            <option value="Blackstone Electric Co">Blackstone Electric Co</option>
                                            <option value="Bluebonnet Electric Cooperative">Bluebonnet Electric Cooperative</option>
                                            <option value="Bounce Energy">Bounce Energy</option>
                                            <option value="Bowie Cass Electric Co-op">Bowie Cass Electric Co-op</option>
                                            <option value="Brilliant Energy">Brilliant Energy</option>
                                            <option value="Brownsville Public Utilities Board">Brownsville Public Utilities Board</option>
                                            <option value="Bryan Texas Utilities">Bryan Texas Utilities</option>
                                            <option value="CenterPoint Energy">CenterPoint Energy</option>
                                            <option value="Central Texas Electric Co-Op">Central Texas Electric Co-Op</option>
                                            <option value="Champion Energy Services">Champion Energy Services</option>
                                            <option value="Cherokee County Electric Cooperative (CCECA)">Cherokee County Electric Cooperative (CCECA)</option>
                                            <option value="Cirro Energy">Cirro Energy</option>
                                            <option value="City of Austin Utilities">City of Austin Utilities</option>
                                            <option value="City of Coleman Utilities">City of Coleman Utilities</option>
                                            <option value="City of Gastonia Electric">City of Gastonia Electric</option>
                                            <option value="City of Sanger Utilities">City of Sanger Utilities</option>
                                            <option value="City of Seymour Electric Distribution Department">City of Seymour Electric Distribution Department</option>
                                            <option value="College Station Utilities">College Station Utilities</option>
                                            <option value="Comanche Electric Cooperative">Comanche Electric Cooperative</option>
                                            <option value="Coserv Electric">Coserv Electric</option>
                                            <option value="CPS Energy">CPS Energy</option>
                                            <option value="Deep East Texas Electric Coop">Deep East Texas Electric Coop</option>
                                            <option value="Denton Municipal Electric">Denton Municipal Electric</option>
                                            <option value="Direct Energy">Direct Energy</option>
                                            <option value="dPi Energy">dPi Energy</option>
                                            <option value="El Paso Electric">El Paso Electric</option>
                                            <option value="Electric Database Publishing">Electric Database Publishing</option>
                                            <option value="Energy Plus Company">Energy Plus Company</option>
                                            <option value="Entergy">Entergy</option>
                                            <option value="Entrust Energy">Entrust Energy</option>
                                            <option value="Everything Energy">Everything Energy</option>
                                            <option value="Fannin County Electric Co">Fannin County Electric Co</option>
                                            <option value="Farmers Electrical Cooperative">Farmers Electrical Cooperative</option>
                                            <option value="Fayette Electric Cooperative">Fayette Electric Cooperative</option>
                                            <option value="First Texas Energy Corporation">First Texas Energy Corporation</option>
                                            <option value="Floresville Electric Light and Power System">Floresville Electric Light and Power System</option>
                                            <option value="Frontier Utilities">Frontier Utilities</option>
                                            <option value="Garland Power and Light">Garland Power and Light</option>
                                            <option value="Georgetown Utility Services (GUS)">Georgetown Utility Services (GUS)</option>
                                            <option value="GEUS Electric">GEUS Electric</option>
                                            <option value="Gexa Energy">Gexa Energy</option>
                                            <option value="Green Mountain Energy">Green Mountain Energy</option>
                                            <option value="Guadalupe Valley Electric Cooperative">Guadalupe Valley Electric Cooperative</option>
                                            <option value="Hamilton County Electric Cooperative">Hamilton County Electric Cooperative</option>
                                            <option value="Heart Of Texas Electric Co-Op">Heart Of Texas Electric Co-Op</option>
                                            <option value="Hilco Electric Co-Op">Hilco Electric Co-Op</option>
                                            <option value="Houston County Electric Cooperative">Houston County Electric Cooperative</option>
                                            <option value="IGS Energy">IGS Energy</option>
                                            <option value="International Electric Power (IEP)">International Electric Power (IEP)</option>
                                            <option value="Jackson EMC">Jackson EMC</option>
                                            <option value="Jasper-Newton Electric Cooperative">Jasper-Newton Electric Cooperative</option>
                                            <option value="Johnson County Electric Cooperative">Johnson County Electric Cooperative</option>
                                            <option value="Just Energy">Just Energy</option>
                                            <option value="Karnes Electric Co-Op">Karnes Electric Co-Op</option>
                                            <option value="Kerrville Public Utility Board (KPUB)">Kerrville Public Utility Board (KPUB)</option>
                                            <option value="Lea County Electric Cooperative">Lea County Electric Cooperative</option>
                                            <option value="Lower Colorado River Authority">Lower Colorado River Authority</option>
                                            <option value="Lubbock Power and Light">Lubbock Power and Light</option>
                                            <option value="Luminant">Luminant</option>
                                            <option value="Lyntegar Electric">Lyntegar Electric</option>
                                            <option value="Magic Valley">Magic Valley</option>
                                            <option value="McCaffety Electric">McCaffety Electric</option>
                                            <option value="Medina Electric Cooperative">Medina Electric Cooperative</option>
                                            <option value="Mid-South Synergy">Mid-South Synergy</option>
                                            <option value="Navarro County Electric Cooperative">Navarro County Electric Cooperative</option>
                                            <option value="Navasota Valley Electric Cooperative">Navasota Valley Electric Cooperative</option>
                                            <option value="New Braunfels Utilities">New Braunfels Utilities</option>
                                            <option value="Nueces Electric Cooperative">Nueces Electric Cooperative</option>
                                            <option value="Oncor Electric (Formerly TXU)">Oncor Electric (Formerly TXU)</option>
                                            <option value="Panola-Harrison Electric Cooperative">Panola-Harrison Electric Cooperative</option>
                                            <option value="Pedernales Electric Cooperative">Pedernales Electric Cooperative</option>
                                            <option value="Pennywise Power">Pennywise Power</option>
                                            <option value="Reliant Energy">Reliant Energy</option>
                                            <option value="Rio Grande Electric Cooperative">Rio Grande Electric Cooperative</option>
                                            <option value="Rusk County Electric Cooperative">Rusk County Electric Cooperative</option>
                                            <option value="Sam Houston Electric">Sam Houston Electric</option>
                                            <option value="San Antonio Public Utilities">San Antonio Public Utilities</option>
                                            <option value="San Bernard Electric Co-Op">San Bernard Electric Co-Op</option>
                                            <option value="San Patricio Electric Co-Op">San Patricio Electric Co-Op</option>
                                            <option value="Sharyland Utilities">Sharyland Utilities</option>
                                            <option value="Source Power and Gas">Source Power and Gas</option>
                                            <option value="South Plains Electric Cooperative">South Plains Electric Cooperative</option>
                                            <option value="Southwestern Electric Power Company (SWEPCO)">Southwestern Electric Power Company (SWEPCO)</option>
                                            <option value="Star Tex Power">Star Tex Power</option>
                                            <option value="Stream Energy">Stream Energy</option>
                                            <option value="Summer Energy">Summer Energy</option>
                                            <option value="Tara Energy">Tara Energy</option>
                                            <option value="Taylor Electric Co-Op">Taylor Electric Co-Op</option>
                                            <option value="Texas Electric Service Company">Texas Electric Service Company</option>
                                            <option value="Tri Eagle Energy">Tri Eagle Energy</option>
                                            <option value="Trinity Valley Electric Cooperative">Trinity Valley Electric Cooperative</option>
                                            <option value="United Cooperative Services">United Cooperative Services</option>
                                            <option value="Upshur Rural Electric Co-Op">Upshur Rural Electric Co-Op</option>
                                            <option value="V247 Power Corportion">V247 Power Corportion</option>
                                            <option value="Victoria Electric Cooperative">Victoria Electric Cooperative</option>
                                            <option value="Wise Electric Co-Op">Wise Electric Co-Op</option>
                                            <option value="Wood County Electric Cooperative">Wood County Electric Cooperative</option>
                                            <option value="WTU Retail Energy">WTU Retail Energy</option>
                                            <option value="XOOM Energy">XOOM Energy</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Utah">
                                        <option value="Brigham City Public Power">Brigham City Public Power</option>
                                            <option value="City of Santa Clara Power">City of Santa Clara Power</option>
                                            <option value="Dixie Escalante Electric">Dixie Escalante Electric</option>
                                            <option value="Garkane Energy Cooperative">Garkane Energy Cooperative</option>
                                            <option value="Heber Light and Power">Heber Light and Power</option>
                                            <option value="Hurricane City Utilities">Hurricane City Utilities</option>
                                            <option value="Hyrum City Power & Light">Hyrum City Power & Light</option>
                                            <option value="Intermountain Power Agency">Intermountain Power Agency</option>
                                            <option value="Kaysville City">Kaysville City</option>
                                            <option value="Lehi City Power">Lehi City Power</option>
                                            <option value="Moon Lake Electric Association">Moon Lake Electric Association</option>
                                            <option value="Murray City Power">Murray City Power</option>
                                            <option value="PacifiCorp (Rocky Mountain Power)">PacifiCorp (Rocky Mountain Power)</option>
                                            <option value="Provo City Power">Provo City Power</option>
                                            <option value="Springville Electric">Springville Electric</option>
                                            <option value="Strawberry Electric Service District">Strawberry Electric Service District</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Virginia">
                                        <option value="A and N Electric Cooperative">A and N Electric Cooperative</option>
                                            <option value="A and N Electric Cooperative (ANEC)">A and N Electric Cooperative (ANEC)</option>
                                            <option value="Allegheny Power">Allegheny Power</option>
                                            <option value="Appalachian Power">Appalachian Power</option>
                                            <option value="Bristol Virginia Utilities">Bristol Virginia Utilities</option>
                                            <option value="Central Virginia Electric Cooperative">Central Virginia Electric Cooperative</option>
                                            <option value="Community Electric Cooperative">Community Electric Cooperative</option>
                                            <option value="Craig-Botetourt Electric Cooperative">Craig-Botetourt Electric Cooperative</option>
                                            <option value="Danville Utilities">Danville Utilities</option>
                                            <option value="Delmarva Power">Delmarva Power</option>
                                            <option value="Dominion Virginia Power">Dominion Virginia Power</option>
                                            <option value="Franklin Electric Department">Franklin Electric Department</option>
                                            <option value="IGS Energy">IGS Energy</option>
                                            <option value="Mecklenburg Electric Cooperative">Mecklenburg Electric Cooperative</option>
                                            <option value="Northern Neck Electric Cooperative">Northern Neck Electric Cooperative</option>
                                            <option value="NOVEC">NOVEC</option>
                                            <option value="Piedmont EMC">Piedmont EMC</option>
                                            <option value="Prince George Electric Cooperative">Prince George Electric Cooperative</option>
                                            <option value="Radford Electric Department">Radford Electric Department</option>
                                            <option value="Rappahannock Electric Cooperative">Rappahannock Electric Cooperative</option>
                                            <option value="Shenandoah Valley Electric Cooperative">Shenandoah Valley Electric Cooperative</option>
                                            <option value="Southside Electric Cooperative">Southside Electric Cooperative</option>
                                            <option value="Town of Richlands Electric Department">Town of Richlands Electric Department</option>
                                            <option value="Town of Vienna">Town of Vienna</option>
                                            <option value="Wakefield Municipal Gas and Lighting">Wakefield Municipal Gas and Lighting</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Vermont">
                                        <option value="Central Vermont Public Service">Central Vermont Public Service</option>
                                            <option value="Green Mountain Power">Green Mountain Power</option>
                                            <option value="Hardwick Electric Department">Hardwick Electric Department</option>
                                            <option value="Vermont Electric Cooperative">Vermont Electric Cooperative</option>
                                            <option value="Washington Electric Cooperative">Washington Electric Cooperative</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Washington">
                                        <option value="Avista Utilities">Avista Utilities</option>
                                            <option value="Benton PUD">Benton PUD</option>
                                            <option value="Chelan County Public Utility District">Chelan County Public Utility District</option>
                                            <option value="City of Blaine">City of Blaine</option>
                                            <option value="Clark Public Utilities">Clark Public Utilities</option>
                                            <option value="Columbia REA">Columbia REA</option>
                                            <option value="Cowlitz PUD">Cowlitz PUD</option>
                                            <option value="Douglas County Public Utility District">Douglas County Public Utility District</option>
                                            <option value="Grant County Public Utility District">Grant County Public Utility District</option>
                                            <option value="Greys Harbour PUD">Greys Harbour PUD</option>
                                            <option value="Inland Power and Light">Inland Power and Light</option>
                                            <option value="Klickitat Public Utility District">Klickitat Public Utility District</option>
                                            <option value="Lewis County Public Works">Lewis County Public Works</option>
                                            <option value="Mason County Public Utility District 3">Mason County Public Utility District 3</option>
                                            <option value="Modern Electric Water Company">Modern Electric Water Company</option>
                                            <option value="Orcas Power and Light Cooperative (OPALCO)">Orcas Power and Light Cooperative (OPALCO)</option>
                                            <option value="Pacific County Public Utility District No. 2">Pacific County Public Utility District No. 2</option>
                                            <option value="PacifiCorp (Pacific Power)">PacifiCorp (Pacific Power)</option>
                                            <option value="Peninsula Light Company">Peninsula Light Company</option>
                                            <option value="Puget Sound Energy">Puget Sound Energy</option>
                                            <option value="Seattle City Light">Seattle City Light</option>
                                            <option value="Snohomish County Public Utility District (PUD)">Snohomish County Public Utility District (PUD)</option>
                                            <option value="Tacoma Power">Tacoma Power</option>
                                            <option value="Tanner Electric">Tanner Electric</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Wisconsin">
                                        <option value="Alliant Energy">Alliant Energy</option>
                                            <option value="Dairyland Power Co-op">Dairyland Power Co-op</option>
                                            <option value="Eau Claire Energy Cooperative">Eau Claire Energy Cooperative</option>
                                            <option value="Kaukauna Utilities">Kaukauna Utilities</option>
                                            <option value="Madison Gas and Electric">Madison Gas and Electric</option>
                                            <option value="North Central Power">North Central Power</option>
                                            <option value="Oakdale Electric Co-op">Oakdale Electric Co-op</option>
                                            <option value="Pierce Pepin Cooperative Services">Pierce Pepin Cooperative Services</option>
                                            <option value="Polk-Burnett Electric Cooperative">Polk-Burnett Electric Cooperative</option>
                                            <option value="Richland Electric Cooperative">Richland Electric Cooperative</option>
                                            <option value="River Falls Municipal Utility">River Falls Municipal Utility</option>
                                            <option value="Scenic Rivers Energy Cooperative">Scenic Rivers Energy Cooperative</option>
                                            <option value="Spooner Municipal Utility">Spooner Municipal Utility</option>
                                            <option value="St. Croix Electric Cooperative (SCEC)">St. Croix Electric Cooperative (SCEC)</option>
                                            <option value="Superior Water Light and Power">Superior Water Light and Power</option>
                                            <option value="Vernon Electrical Cooperative">Vernon Electrical Cooperative</option>
                                            <option value="Village of Hustisford Electric">Village of Hustisford Electric</option>
                                            <option value="We Energies">We Energies</option>
                                            <option value="Westby Electric Department">Westby Electric Department</option>
                                            <option value="Wisconsin Power and Light Company">Wisconsin Power and Light Company</option>
                                            <option value="Wisconsin Public Service Corporation">Wisconsin Public Service Corporation</option>
                                            <option value="Xcel Energy">Xcel Energy</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="West Virginia">
                                        <option value="Allegheny Power">Allegheny Power</option>
                                            <option value="Appalachian Power">Appalachian Power</option>
                                            <option value="Wheeling Electric Power (AEP Ohio)">Wheeling Electric Power (AEP Ohio)</option>
                                        <option value="Other">Other</option>
                                    </optgroup>
                                            <optgroup label="Wyoming">
                                        <option value="Cheyenne Light Fuel & Power">Cheyenne Light Fuel & Power</option>
                                            <option value="High Plains Power">High Plains Power</option>
                                            <option value="High West Energy">High West Energy</option>
                                            <option value="Lower Valley Energy">Lower Valley Energy</option>
                                            <option value="PacifiCorp (Rocky Mountain Power)">PacifiCorp (Rocky Mountain Power)</option>
                                            <option value="Powder River Energy Corporation">Powder River Energy Corporation</option>
                                            <option value="Wheatland Rural Electric Association">Wheatland Rural Electric Association</option>
                                        <option value="Other">Other</option>
                                    </optgroup>

                </select>
                <span class="input-group-btn"><b class="btn btn-lg btn-primary" style="width:150px;">Utility Company</b></span>
            </div>            
 <script>
var state_province = $('#curt_utl option, #curt_utl optgroup'); 
state_province.hide(); 


 $('#state').change(function(){
     state_province.hide(); 
     $("#curt_utl optgroup[label='"+$(this).find(':selected').html() + "']" )
     .children()
     .andSelf()
     .show();  
 }); 
  </script>
  <div class="input-group" style="width:340px;text-align:center;margin:0 auto;">
                <select class="form-control input-lg" required="" name="shade">
                    <option value="No Shade">No Shade</option>
                    <option value="A Little Shade">A Little Shade</option>
                    <option value="A Lot Of Shade">A Lot Of Shade</option>
                    <option value="Uncertian">Uncertain</option>                    
                </select>
                <span class="input-group-btn"><b class="btn btn-lg btn-primary" >Shade Type</b></span>
            </div>
            <div class="input-group" style="width:340px;text-align:center;margin:0 auto;">
                <select class="form-control input-lg" required="" name="credit">
                    <option value="635">Fair (625 - 649)</option>
                    <option value="680">Good (650 - 699)</option>
                    <option value="701">Excellent (700+)</option>
                </select>
                <span class="input-group-btn"><b class="btn btn-lg btn-primary" >Credit</b></span>
            </div>
            <div class="input-group" name="homeowner" style="width:340px;text-align:center;margin:0 auto;">
               
              <div class="btn btn-lg btn-primary active" id="home1" value="Yes">Yes</div>
              <div class="btn btn-lg btn-default" id="home2" value="No">No</div>
              <input id="homeowner" style="display:none" required="" type="text" name="homeowner" value="Yes">
            <span class="input-group-btn"><b class="btn btn-lg btn-primary" >Home Owner</b></span>
            </div>

 <!-- === HIDDEN FIELDS ===--> 
            <input type="hidden" name="aid" value="<?php if($_REQUEST['aid']){ echo $_REQUEST['aid'];} ?>">
            <input type="hidden" name="sub_id" value="<?php if($_REQUEST['sub_id']){ echo $_REQUEST['sub_id'];} ?>">
            <input id="leadid_token" name="leadID" type="hidden" value=""/>
              <input type="hidden" name="submitted" value="No">
              <p></p>
              <button class="btn btn-lg btn-success" type="submit">Submit</button>
          </form>
<script id="LeadiDscript" type="text/javascript">
// <!--
(function() {
var s = document.createElement('script');
s.id = 'LeadiDscript_campaign';
s.type = 'text/javascript';
s.async = true;
s.src = '//create.lidstatic.com/campaign/fee461c6-221c-d5f5-a4dd-20ee10bb09fd.js?snippet_version=2&f=reset';
var LeadiDscript = document.getElementById('LeadiDscript');
LeadiDscript.parentNode.insertBefore(s, LeadiDscript);
})();
// -->
</script>
<noscript><img src='//create.leadid.com/noscript.gif?lac=4bf9a5fe-53cc-bbbf-f68b-698aa3bd046a&lck=fee461c6-221c-d5f5-a4dd-20ee10bb09fd&snippet_version=2' /></noscript>          
<script id="LeadiDscript" type="text/javascript">
// <!--
(function() {
var s = document.createElement('script');
s.id = 'LeadiDscript_campaign';
s.type = 'text/javascript';
s.async = true;
s.src = '//create.lidstatic.com/campaign/fee461c6-221c-d5f5-a4dd-20ee10bb09fd.js?snippet_version=2';
var LeadiDscript = document.getElementById('LeadiDscript');
LeadiDscript.parentNode.insertBefore(s, LeadiDscript);
})();
// -->
</script>
<noscript><img src='//create.leadid.com/noscript.gif?lac=4bf9a5fe-53cc-bbbf-f68b-698aa3bd046a&lck=fee461c6-221c-d5f5-a4dd-20ee10bb09fd&snippet_version=2' /></noscript>

        </div>
        
      </div> <!-- /row -->
  
  	  <div class="row">
       
        <div class="col-lg-12 text-center v-center" style="font-size:39pt;">
          <a href="#"><i class="icon-google-plus"></i></a> <a href="#"><i class="icon-facebook"></i></a>  <a href="#"><i class="icon-twitter"></i></a> <a href="#"><i class="icon-github"></i></a> <a href="#"><i class="icon-pinterest"></i></a>
        </div>
      
      </div>
  
  	<br><br><br>

</div> <!-- /container full -->

<div class="container">
  
  	<hr>
  
<!--  	<div class="row">
        <div class="col-md-4">
          <div class="panel panel-default">
            <div class="panel-heading"><h3>Hello.</h3></div>
            <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. 
            Quisque mauris augue, molestie tincidunt condimentum vitae, gravida a libero. Aenean sit amet felis 
            dolor, in sagittis nisi. Sed ac orci quis tortor imperdiet venenatis. Duis elementum auctor accumsan. 
            Aliquam in felis sit amet augue.
            </div>
          </div>
        </div>
      	<div class="col-md-4">
        	<div class="panel panel-default">
            <div class="panel-heading"><h3>Hello.</h3></div>
            <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. 
            Quisque mauris augue, molestie tincidunt condimentum vitae, gravida a libero. Aenean sit amet felis 
            dolor, in sagittis nisi. Sed ac orci quis tortor imperdiet venenatis. Duis elementum auctor accumsan. 
            Aliquam in felis sit amet augue.
            </div>
          </div>
        </div>
      	<div class="col-md-4">
        	<div class="panel panel-default">
            <div class="panel-heading"><h3>Hello.</h3></div>
            <div class="panel-body">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis pharetra varius quam sit amet vulputate. 
            Quisque mauris augue, molestie tincidunt condimentum vitae, gravida a libero. Aenean sit amet felis 
            dolor, in sagittis nisi. Sed ac orci quis tortor imperdiet venenatis. Duis elementum auctor accumsan. 
            Aliquam in felis sit amet augue.
            </div>
          </div>
        </div>
    </div>-->
  
	<div class="row">
        <div class="col-lg-12">
        <br><br>
          <p class="pull-right"><a href="http://www.squeezemyleads.com">Squeeze Media</a> &nbsp; ©Copyright 2015 <sup>TM</sup></p>
        <br><br>
        </div>
    </div>
</div>
    </body>
    
    <script>
$('#home1 , #home2').click(function(){
$(this).parent().children('.active').removeClass('active');
$(this).addClass('active');

$(this).parent().children('.btn-primary').addClass('btn-default').removeClass('btn-primary');
$(this).addClass('btn-primary').removeClass('btn-default');
$('#homeowner').val($(this).text());
});

</script>
</html>


