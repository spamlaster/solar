<?php

  if (isset($_POST["submit"])) {
        $name = $_POST['name'];
        $email = $_POST['email'];
        $message = $_POST['message'];
        $human = intval($_POST['human']);
        $from = $name.'mysolarliving'; 
        $to = 'nick@lightsonmedia.com'; 
        $subject = 'Message from My Solar Living ';
        
        $body = "From: $name\n E-Mail: $email\n Message:\n $message";
 
        // Check if name has been entered
        if (!$_POST['name']) {
            $errName = 'Please enter your name';
        }
        
        // Check if email has been entered and is valid
        if (!$_POST['email'] || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
            $errEmail = 'Please enter a valid email address';
        }
        
        //Check if message has been entered
        if (!$_POST['message']) {
            $errMessage = 'Please enter your message';
        }
        //Check if simple anti-bot test is correct
        if ($human !== 5) {
            $errHuman = 'Your anti-spam is incorrect';
        }
 
// If there are no errors, send the email
if (!$errName && !$errEmail && !$errMessage && !$errHuman) {
    if (mail ($to, $subject, $body, $from)) {
        $result='<div class="alert alert-success">Thank You! I will be in touch</div>';
    } else {
        $result='<div class="alert alert-danger">Sorry there was an error sending your message. Please try again later</div>';
    }
}
    }
    
?>

<!DOCTYPE html>
<html lang="en">
        <head>
            <!-- Meta, title, CSS, favicons, etc. -->
            <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1.0">
                <meta name="description" content="">
                <meta name="author" content="">
        
                <title>My Solar Living Contact</title>
        
                <!-- Bootstrap core CSS -->
                <link href="../assets/css/bootstrap.min.css" rel="stylesheet">
                
                <!-- Fonts -->
                <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,700,400italic,700italic' rel='stylesheet' type='text/css'>
                
                <!-- Rocket extras -->
                <link href="../assets/css/animate.css" rel="stylesheet">
                <link href="../assets/css/prettyPhoto.css" rel="stylesheet">
                <link href="../assets/css/style.css" rel="stylesheet">
                <!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
                
                <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
                <!--[if lt IE 9]>
                  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
                <![endif]-->
                
                <!-- Favicons -->
                <link rel="shortcut icon" href="../assets/favicon.png">
        </head>
        <body>

        <!-- Start Rocket -->
        <!-- ********************* -->

        <!-- Parallax Background
        ================================================== -->
        <!-- image is set in the CSS as a background image -->
        <div id="parallax"></div>
        <!-- End Parallax Background
        ================================================== -->

        <!-- Start Header
        ================================================== -->
        <header id="header" class="navbar navbar-inverse navbar-fixed-top" role="banner">
          <div class="container">
            <div class="navbar-header">
              <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <!-- Your Logo -->
              <div style="height: 20px;">
              <a href="../" class="navbar-brand imglogo"><img  src="../assets/images/solar_logo2.png"></a>
              </div>
            </div>
          <!-- Start Navigation -->
            <nav class="collapse navbar-collapse bs-navbar-collapse navbar-right" role="navigation">
              <ul class="nav navbar-nav">
                <li>
                  <a href="../">Home</a>
                </li>
              </ul>
              <ul class="nav navbar-nav navbar-right header-button">

              </ul>
            </nav>
          </div>
        </header>
        <!-- ==================================================
        End Header -->

        <!-- Start Hero Section
        ================================================== -->
        <section id="hero" class="section">
                <div class="container">
                        <div class="row">
                                <div class="col-md-5"></div>
                                <div class="col-md-7">

                                        <div class="lp-element animate" data-animate="">

                                                
                                        </div>
                                </div>
                        </div>
                </div>
        </section>


        <!-- Start Image Block
        ================================================== -->
        <section id="image" class="section" style="background-color:#FFF; border-top: 1px solid rgba(0, 0, 0, .1);">
                <div class="container">
                        <div class="row">
                                <div class="col-md-12 sol-sm-12">
                                        <div class="overview animate" data-animate="flipInX">
                                                <h1><strong>About Us</strong></h1>
                                                <p class="lead">We are qualified experts that specialize in connecting homeowners with solar contractors in their area. Our goal is to cut costs to the homeowner by connecting them with top quality installers that will best fit their needs. We care about the environment and know that as more people turn to solar, not only will it save them money, but it will help preserve Earth's natural resources.</p>
                                        </div>
                                </div>
                                <div class="col-md-6 col-sm-12"><img class="animate img-responsive img-rounded" data-animate="fadeInLeft" src="../assets/images/const_solar.jpg" alt=""/></div>
                                <div class="col-md-6 col-sm-12">
                                        <div class="content-block animate" data-animate="fadeInRight">
<!--                                                <h2>We can help</h2>
                                                <ul>
                                                    <li>Installing solar panels can eliminate between 3 and 4 tons of carbon in just one year.</li>
                                                    <li>The United States produces 3% of the world's oild output, while consuming a staggering 25%.</li>
                                                    <li>The sun is our most dependable source of energy!</li>
                                                </ul>-->
                                                <hr />
                                                <!--<p><a href="../">Sign Up</a> <strong>Now</strong>  <small>This line of text is meant to be treated as fine print.</small> synergistic technologies. Holisticly seize professional ideas before wireless metrics. Appropriately drive <strong>rendered as bold text</strong> progressive <abbr title="attribute">attr</abbr> catalysts <abbr title="HyperText Markup Language" class="initialism">HTML</abbr>.</p>-->
                                                
<form class="form-horizontal" role="form" method="post" action="index.php">
    <div class="form-group">
        <label for="name" class="col-sm-2 control-label">Name</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="name" name="name" placeholder="First & Last Name" value="<?php echo htmlspecialchars($_POST['name']); ?>">
            <?php echo "<p class='text-danger'>$errName</p>";?>
        </div>
    </div>
    <div class="form-group">
        <label for="email" class="col-sm-2 control-label">Email</label>
        <div class="col-sm-10">
            <input type="email" class="form-control" id="email" name="email" placeholder="example@domain.com" value="<?php echo htmlspecialchars($_POST['email']); ?>">
            <?php echo "<p class='text-danger'>$errEmail</p>";?>
        </div>
    </div>
    <div class="form-group">
        <label for="message" class="col-sm-2 control-label">Message</label>
        <div class="col-sm-10">
            <textarea class="form-control" rows="4" name="message"><?php echo htmlspecialchars($_POST['message']);?></textarea>
            <?php echo "<p class='text-danger'>$errMessage</p>";?>
        </div>
    </div>
    <div class="form-group">
        <label for="human" class="col-sm-2 control-label">2 + 3 = ?</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="human" name="human" placeholder="Your Answer">
            <?php // echo "<p class='text-danger'>$errHuman</p>";?>
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <input id="submit" name="submit" type="submit" value="Send" class="btn btn-primary">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-10 col-sm-offset-2">
            <?php echo $result; ?>
        </div>
    </div>
</form>
                                        </div>
                                </div>
                        </div>
                </div>
        </section>
        <!-- ==================================================
        End Image Blcok -->

        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="../assets/js/jquery-1.10.2.min.js"></script>
        <script src="../assets/js/bootstrap.js"></script>
        <script src="../assets/js/waypoints.min.js"></script>
        <script src="../assets/js/jquery.scrollto.min.js"></script>
        <script src="../assets/js/jquery.localscroll.min.js"></script>
        <script src="../assets/js/jquery.prettyPhoto.js"></script>
        <script src="../assets/js/scripts.js"></script>


        <!-- ********************* -->
        <!-- Start Rocket -->

        </body>
</html>
                                                   