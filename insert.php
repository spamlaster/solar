<?php

error_reporting(E_ALL);
ini_set("display_errors", 1);

//-- Set timezone to MDT --//
date_default_timezone_set('America/Denver');

$con=mysqli_connect("localhost","nick","password","squeeze");
// Check connection
if (mysqli_connect_errno())
  {
  echo "Failed to connect to MySQL: " . mysqli_connect_error();
  }

//-- Function to Remove All Non-Numeric Characters --//
function keepNumbers($input){
	//echo $input.'<br /><br />';
	$input = preg_replace("/[^0-9]+/", "", $input);
	//echo $input.'<br />';
	return $input;
}
//-- Function to Truncate value length to specific size --//
function truncateLength($input, $len){
	$input = strval($input);
	$input = substr($input, 0, $len);
	return $input;
}

function cleanInput($input) {
 
  $search = array(
    '@<script[^>]*?>.*?</script>@si',   // Strip out javascript
    '@<[\/\!]*?[^<>]*?>@si',            // Strip out HTML tags
    '@<style[^>]*?>.*?</style>@siU',    // Strip style tags properly
    '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
  );
 
    $output = preg_replace($search, '', $input);
    return $output;
  }  
      
function getGUID(){
    if (function_exists('com_create_guid')){
        return com_create_guid();
    }else{
        mt_srand((double)microtime()*10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid = chr(123)// "{"
            .substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid,12, 4).$hyphen
            .substr($charid,16, 4).$hyphen
            .substr($charid,20,12)
            .chr(125);// "}"
        return $uuid;
    }
}
//$GUID = getGUID();
  $GUID = uniqid(rand());
  
$first_name = preg_replace('#[^A-Za-z0-9]#i','', $_POST["first_name"]);
$first_name = mysqli_real_escape_string($con, ucfirst($first_name));

$last_name = preg_replace('#[^A-Za-z0-9]#i','', $_POST["last_name"]);
$last_name = mysqli_real_escape_string($con, ucfirst($last_name));
  
$address = $_POST['address'];
$address = mysqli_real_escape_string($con, $address);
  
$sign_in = date('Y-m-d H:i:s');
$is_signin = 'Y';
$pin = md5(filter_input(INPUT_POST, 'pin'));
$ip_address = $_SERVER['REMOTE_ADDR'];
//$pin = filter_input(INPUT_POST, 'pin');
//echo $first_name . $last_name . $company;

//clean email field and place NA if empty
$email = empty($_REQUEST['email']) ? 'NA' : trim($_REQUEST['email']);  
$email = filter_var($email, FILTER_SANITIZE_EMAIL);
$email = mysqli_real_escape_string($con, $email);

//vertial input
$vertical = 'Solar';


//vertial input
$submitted = 'No';

// -- Insert info into database -- //
$sql="INSERT INTO solar (leadid, first_name, last_name, address, city, state, zip, email, phone, bill, home_owner, shade, submitted, vertical, ip_address)

VALUES
    ('$GUID',
    '$first_name',
    '$last_name',
    '".mysqli_real_escape_string($con, (isset($_REQUEST['address']) ? $_REQUEST['address'] : ''))."',
    '".mysqli_real_escape_string($con, (isset($_REQUEST['city']) ? $_REQUEST['city'] : ''))."', 
    '".mysqli_real_escape_string($con, (isset($_REQUEST['state']) ? $_REQUEST['state'] : ''))."',  
    '".mysqli_real_escape_string($con, (isset($_REQUEST['zip']) ? $_REQUEST['zip'] : ''))."',
    '".mysqli_real_escape_string($con, (isset($_REQUEST['email']) ? $_REQUEST['email'] : ''))."',
    '".mysqli_real_escape_string($con, (isset($_REQUEST['phone']) ? $_REQUEST['phone'] : ''))."',
    '".mysqli_real_escape_string($con, (isset($_REQUEST['bill']) ? $_REQUEST['bill'] : ''))."',
    '".mysqli_real_escape_string($con, (isset($_REQUEST['home_owner']) ? $_REQUEST['home_owner'] : ''))."',    
    '".mysqli_real_escape_string($con, (isset($_REQUEST['shade']) ? $_REQUEST['shade'] : ''))."',
    '$submitted',
    '$vertical',    
    '$ip_address')";
	
if (!mysqli_query($con,$sql))
  {
  die('Error: ' . mysqli_error($con));
  }
  var_dump($sql);
  
header('Location: thank/');

  
mysqli_close($con);
 ?>
