angular.module('CaseStatusApp.controllers', []).
  controller('caseController', function($scope, myService) {
      
request = myService.getCases();
request.then(
  function(payload) {
    
    $scope.cases = payload.data;
    var len = $scope.cases.length;
    var issue = [0,0,0,0],need = [0,0,0,0],code = [0,0,0,0],ready = [0,0,0,0],passed = [0,0,0,0],time;
    
    for (var i=0; i<len; i++){
    time =  Math.floor((Date.now() - Date.parse($scope.cases[i].date_case_modified))/(60000*60*24));
    if(time < 7){
        if($scope.cases[i].status == 'Eng - New Issue'){
            issue[0] += 1;
        }if($scope.cases[i].status == 'Eng - Need more info'){
            need[0] += 1;
        }if($scope.cases[i].status == 'Eng - Code Review'){
            code[0] += 1;
        }if($scope.cases[i].status == 'Eng - Ready for Dev QA'){
            ready[0] += 1;
        }if($scope.cases[i].status == 'Eng - Passed Dev QA'){
            passed[0] += 1;
        }
    }else if(time < 14){
        if($scope.cases[i].status == 'Eng - New Issue'){
            issue[1] += 1;
        }if($scope.cases[i].status == 'Eng - Need more info'){
            need[1] += 1;
        }if($scope.cases[i].status == 'Eng - Code Review'){
            code[1] += 1;
        }if($scope.cases[i].status == 'Eng - Ready for Dev QA'){
            ready[1] += 1;
        }if($scope.cases[i].status == 'Eng - Passed Dev QA'){
            passed[1] += 1;
        }
    }else if(time < 21){
        if($scope.cases[i].status == 'Eng - New Issue'){
            issue[2] += 1;
        }if($scope.cases[i].status == 'Eng - Need more info'){
            need[2] += 1;
        }if($scope.cases[i].status == 'Eng - Code Review'){
            code[2] += 1;
        }if($scope.cases[i].status == 'Eng - Ready for Dev QA'){
            ready[2] += 1;
        }if($scope.cases[i].status == 'Eng - Passed Dev QA'){
            passed[2] += 1;
        }
    }else{
        if($scope.cases[i].status == 'Eng - New Issue'){
            issue[3] += 1;
        }if($scope.cases[i].status == 'Eng - Need more info'){
            need[3] += 1;
        }if($scope.cases[i].status == 'Eng - Code Review'){
            code[3] += 1;
        }if($scope.cases[i].status == 'Eng - Ready for Dev QA'){
            ready[3] += 1;
        }if($scope.cases[i].status == 'Eng - Passed Dev QA'){
            passed[3] += 1;
        }
    }
    
    }
     angular.forEach($scope.cases, function (el) {
            var time =  Math.floor((Date.now() - Date.parse(el.date_case_modified))/(60000*60*24));
            el.duration = time;
        });

    generateGraphic(issue,need,code,ready,passed);
  });
 
});