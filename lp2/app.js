angular.module('CaseStatusApp', [
    'CaseStatusApp.directives',
  'CaseStatusApp.controllers',
  'CaseStatusApp.services',
  'ngRoute'
]).
config(['$routeProvider',
        function($routeProvider) {
            $routeProvider.
                when('/', {
                    templateUrl: '/partials/partial.php',
                    controller: 'caseController'
                }).
                otherwise({
                    redirectTo: '/'
                });
        }]);