<div id="outterContainer">
    <span>Search: <input id="search" ng-model="searchText"></span>
 <div id='innerContainer'>
     
<div id="container" class="row" ng-repeat="case in cases | filter:searchText">
    <div id="case_id"><a href="https://sta.insidesales.com/cases/case?case_id={{case.case_id}}" target="_blank">{{case.case_id}}</a></div>
    <div id="value">{{case.value}}</div>
    <div id="status">{{case.status}}</div>
    <div id="days">{{case.duration + ' Days'}}</div>
</div>
</div>
</div>
