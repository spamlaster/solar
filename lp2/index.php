<!DOCTYPE html>
<html ng-app="CaseStatusApp">
    <head>
        <meta charset="UTF-8">
        <title>Jared's Dashboard</title>
        <link rel="stylesheet" type="text/css" href="css/styles.css">
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.5/angular.min.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.3.5/angular-route.min.js"></script>
        <script src="http://d3js.org/d3.v3.min.js" charset="utf-8"></script>
    </head> 
    <body>
        <svg class="chart"></svg>
        <div case></div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script src="app.js"></script>
        <script src="controllers/caseController.js"></script>
        <script src="directives/caseDirective.js"></script>
        <script src="services/caseService.js"></script>
        <script src="scripts/d3.js"></script>
    </body>   
</html>  
