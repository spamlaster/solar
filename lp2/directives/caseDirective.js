angular.module('CaseStatusApp.directives', []).
directive('case', function() {
  return {
    restrict: 'A',
    controller: 'caseController',
    templateUrl: 'partials/partial2.php'
  };
});