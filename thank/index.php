<!DOCTYPE html>
<html lang="en">
	<head>
	    <!-- Meta, title, CSS, favicons, etc. -->
	    <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="">
		<meta name="author" content="">
	
		<title>
		  
		    Promo
		  
		</title>
	
		<!-- Bootstrap core CSS -->
		<link href="../assets/css/bootstrap.min.css" rel="stylesheet">
		
		<!-- Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,700,400italic,700italic' rel='stylesheet' type='text/css'>
		
		<!-- Rocket extras -->
		<link href="../assets/css/animate.css" rel="stylesheet">
		<link href="../assets/css/prettyPhoto.css" rel="stylesheet">
		<link href="../assets/css/style.css" rel="stylesheet">
		<!--[if lt IE 9]><script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
		
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
		  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
		<![endif]-->
		
		<!-- Favicons -->
		<link rel="shortcut icon" href="../assets/favicon.png">
	</head>
<style>
    ul
{
list-style-type: none;
}
</style>            
	<body>
	
	<!-- Start Rocket -->
	<!-- ********************* -->
	
	<!-- Parallax Background
	================================================== -->
	<!-- image is set in the CSS as a background image -->
	<div id="parallax"></div>
	<!-- End Parallax Background
	================================================== -->
	
	
	<!-- Start Gallery Block
	================================================== -->
	<section id="gallery" class="section">
		<div class="container">
			<div class="row">
				<div class="col-md-12 sol-sm-12">
					<div class="overview animate" data-animate="flipInY">
						<h1><strong>Thank You</strong> for requesting a free home solar quote. </h1>
                                                <p class="lead">One of our representatives will be in contact with you shortly. </p>
                                                <h2>Next Steps</h2>
                                                <ul>
                                                    <li><span class="glyphicon glyphicon-ok"></span>&nbsp; We are matching your information with our top solar suppliers. Who will be reaching out to you in the next 24 to 48 hours. </li>
                                                <li><span class="glyphicon glyphicon-ok"></span>&nbsp; We will be sending you an email with the best matched suppliers. Please check your email.  </li>
                                                </ul>
                                        </div>
                                    <div class="row">
                                        <hr>
                                        <p>You also may be interested in these below</p><br>
                                    </div>
				</div>
				<div class="col-md-6 col-sm-4 col-xs-6">
                                    <a href="http://omtrk.go2cloud.org/aff_c?offer_id=84&aff_id=2" title="Refinance Offer">
						<img src="../assets/images/refi_600x200.png" alt="Turtle" class="img-rounded image-responsive animate" data-animate="fadeIn">
					</a>
				</div>
				<div class="col-md-6 col-sm-4 col-xs-6">
					<a href="http://placehold.it/600x200" title="This is the description">
						<img src="http://placehold.it/600x200" alt="Turtle" class="img-rounded image-responsive animate" data-animate="fadeIn">
					</a>
				</div>
				<div class="col-md-6 col-sm-4 col-xs-6">
					<a href="http://placehold.it/600x200" title="This is the description">
						<img src="http://placehold.it/600x200" alt="Turtle" class="img-rounded image-responsive animate" data-animate="">
					</a>
				</div>
				<div class="col-md-6 col-sm-4 col-xs-6">
					<a href="http://placehold.it/600x200" title="This is the description">
						<img src="http://placehold.it/600x200" alt="Turtle" class="img-rounded image-responsive animate" data-animate="">
					</a>
				</div>
				
			</div>
		</div>
	</section>
	
<!-- Offer Conversion: My Solar Living -->
<iframe src="http://squeezemedia.go2cloud.org/aff_l?offer_id=3" scrolling="no" frameborder="0" width="1" height="1"></iframe>
<!-- // End Offer Conversion -->
	
	
	<!-- Bootstrap core JavaScript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="../assets/js/jquery-1.10.2.min.js"></script>
	<script src="../assets/js/bootstrap.js"></script>
	<script src="../assets/js/waypoints.min.js"></script>
	<script src="../assets/js/jquery.scrollto.min.js"></script>
	<script src="../assets/js/jquery.localscroll.min.js"></script>
	<script src="../assets/js/jquery.prettyPhoto.js"></script>
	<script src="../assets/js/scripts.js"></script>
	
	
	<!-- ********************* -->
	<!-- Start Rocket -->
	
	</body>
</html>
